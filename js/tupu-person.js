

/** 网页当前状态判断 (解决没布局完就切换页面造成点聚集在一起)*/
var hidden, state, visibilityChange;
if (typeof document.hidden !== "undefined") {
    hidden = "hidden";
    visibilityChange = "visibilitychange";
    state = "visibilityState";
} else if (typeof document.mozHidden !== "undefined") {
    hidden = "mozHidden";
    visibilityChange = "mozvisibilitychange";
    state = "mozVisibilityState";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
    state = "msVisibilityState";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
    state = "webkitVisibilityState";
}
// 添加监听器，在title里显示状态变化

/** 解决浏览器标签切换排列问题 */
var _isNeedReload = false;
var _isGraphLoaded = false;
document.addEventListener(visibilityChange, function() {

    if(document[state] == 'visible'){
        if(_isNeedReload){
            $("#MainCy").html('');
            $('#TrTxt').removeClass('active');
            /*getData(_currentKeyNo);*/   /*keyNo字段取消*/
            getData(_currentName);
        }
        //document.title = 'hidden-not-loaded'
    } else {
        if(!_isGraphLoaded){
            _isNeedReload = true;
        }
    }
}, false);
/** end 解决浏览器标签切换排列问题 */


/** end 网页当前状态判断 */

    //

var cy;
var id;
var activeNode;

var _rootData,_rootNode;

var _COLOR = {
    //node :   {person: '#09ACB2',company:'#128BED',current:'#FD485E'},
    //node :   {person: '#20BDBF',company:'#4EA2F0',current:'#FD485E'},
    node :   {person: '#FD485E',company:'#4ea2f0',current:'#ff9e00'},
    //node :   {person: '#a177bf',company:'#4ea2f0',current:'#FD485E'},
    //node :   {person: '#f2af00',company:'#0085c3',current:'#7ab800'},
    //border : {person: '#09ACB2',company:'#128BED',current:'#FD485E'},
    //border : {person: '#57A6A8',company:'#128BED',current:'#FD485E'},
    border : {person: '#FD485E',company:'#128BED',current:'#EF941B'},
    //border : {person: '#7F5AB8',company:'#128BED',current:'#FD485E'},
    //border : {person: '#f2af00',company:'#0085c3',current:'#7ab800'},
    //line:    {invest:'#128BED',employ:'#FD485E',legal:'#09ACB2'},
    //line:    {invest:'#4EA2F0',employ:'#20BDBF',legal:'#D969FF'}
    line:    {invest:'#fd485e',employ:'#4ea2f0',legal:'#4ea2f0'}
    //line:    {invest:'#e43055',employ:'#a177bf',legal:'#4ea2f0'}
};
var _currentKeyNo,_companyRadius = 35,_personRadius = 15,_circleMargin = 10,_circleBorder = 3,
    _layoutNode = {}, _isFocus = false,_currentName;
var _maxChildrenLength = 0;


/****** 工具 ******/

//去重操作,元素为对象
/*array = [
    {a:1,b:2,c:3,d:4},
    {a:11,b:22,c:333,d:44},
    {a:111,b:222,c:333,d:444}
];
var arr = uniqeByKeys(array,['a','b']);*/
function uniqeByKeys(array,keys){
    //将对象元素转换成字符串以作比较
    function obj2key(obj, keys){
        var n = keys.length,
            key = [];
        while(n--){
            key.push(obj[keys[n]]);
        }
        return key.join('|');
    }

    var arr = [];
    var hash = {};
    for (var i = 0, j = array.length; i < j; i++) {
        var k = obj2key(array[i], keys);
        if (!(k in hash)) {
            hash[k] = true;
            arr .push(array[i]);
        }
    }
    return arr ;
};
//去重操作,普通元素
Array.prototype.unique = function(){
    var res = [];
    var json = {};
    for(var i = 0; i < this.length; i++){
        if(!json[this[i]]){
            res.push(this[i]);
            json[this[i]] = 1;
        }
    }
    return res;
};

//深复制对象方法
function cloneObj (obj) {
    var newObj = {};
    if (obj instanceof Array) {
        newObj = [];
    }
    for (var key in obj) {
        var val = obj[key];
        //newObj[key] = typeof val === 'object' ? arguments.callee(val) : val; //arguments.callee 在哪一个函数中运行，它就代表哪个函数, 一般用在匿名函数中。
        newObj[key] = typeof val === 'object' ? cloneObj(val): val;
    }
    return newObj;
};

/****** 数据处理 ******/

// 数据处理：将原始数据转换成graph数据
function getRootData(list) {
    var graph = {}
    graph.nodes = [];
    graph.links = [];
    //graph.nodes

        var nodes = list.nodes;
        for(var j = 0; j < nodes.length; j++){
            var node = nodes[j];
            var o = {};
            o.nodeId = node.id;
            o.data = {};
            o.data.obj = node;
            //o.data.showStatus = 'NORMAL'; // NORMAL HIGHLIGHT DULL
            o.data.showStatus = null; // NORMAL HIGHLIGHT DULL
            o.layout = {}
            o.layout.level = null; // 1 当前查询节点
            o.layout.singleLinkChildren = []; // 只连接自己的node
            graph.nodes.push(o);

            // add by chenez for  at 2018/9/25 14:50 start
            // 设置_rootNode
            /*if (_currentKeyNo == o.data.obj.properties.keyNo){
                _rootNode = o;
            }*/
            _rootNode = o;
            // add by chenez for  at 2018/9/25 14:50 end

        }

    graph.nodes = uniqeByKeys(graph.nodes,['nodeId']);

    //graph.links
        var relationships = list.relationships;
        for(var k = 0; k < relationships.length; k++) {
            var relationship = relationships[k];
            var o = {}
            o.data = {};
            o.data.obj = relationship;
            //o.data.showStatus = 'NORMAL'; // NORMAL HIGHLIGHT DULL
            o.data.showStatus = null; // NORMAL HIGHLIGHT DULL
            o.sourceNode = getGraphNode(relationship.startNode,graph.nodes);
            o.targetNode = getGraphNode(relationship.endNode,graph.nodes);
            o.linkId = relationship.id;
            o.source = getNodesIndex(relationship.startNode,graph.nodes);
            o.target = getNodesIndex(relationship.endNode,graph.nodes);

            graph.links.push(o);
        }

    graph.links = uniqeByKeys(graph.links,['linkId']);


    //emplyRevert(graph.links);
    //mergeLinks(graph.links);
    setLevel(graph.nodes,graph.links);
    setCategoryColor(graph.nodes,graph.links);
    return graph;
}
// 数据处理：董监高箭头翻转
/*function emplyRevert(links) {
    links.forEach(function (link,i) {
        if(link.data.obj.type == 'EMPLOY'){
            var tmpObj = link.source;
            var tmpObjNode = link.sourceNode;
            link.source = link.target;
            link.sourceNode = link.targetNode;
            link.target = tmpObj;
            link.targetNode = tmpObjNode;
        }
    });
}*/
// 数据处理：董监高、法人线合并
function mergeLinks(links) {
    links.forEach(function (link,i) {
        if(link.sourceNode.data.obj.labels[0] == 'Person' && link.data.obj.type == 'LEGAL'){
            links.forEach(function (nextLink,j) {
                if(link.linkId != nextLink.linkId &&
                    link.sourceNode.nodeId == nextLink.sourceNode.nodeId &&
                    link.targetNode.nodeId == nextLink.targetNode.nodeId &&
                    nextLink.data.obj.type == 'EMPLOY'){

                    links.splice(j,1);
                }
            });
        }

        if(link.sourceNode.data.obj.labels[0] == 'Person' && link.data.obj.type == 'EMPLOY'){
            links.forEach(function (nextLink,j) {
                if(link.linkId != nextLink.linkId &&
                    link.sourceNode.nodeId == nextLink.sourceNode.nodeId &&
                    link.targetNode.nodeId == nextLink.targetNode.nodeId &&
                    nextLink.data.obj.type == 'LEGAL'){

                    links.splice(j,1);
                }
            });
        }
    });
//        console.log(links);
}
// 数据处理：设置节点层级
function setLevel(svg_nodes,svg_links) {
    function getNextNodes(nodeId,links,parentLevel){
        var nextNodes = [];
        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if(nodeId == link.sourceNode.nodeId && !link.targetNode.layout.level){
                link.targetNode.layout.level = parentLevel;
                nextNodes.push(link.targetNode);
            } else if (nodeId == link.targetNode.nodeId && !link.sourceNode.layout.level) {
                link.sourceNode.layout.level = parentLevel;
                nextNodes.push(link.sourceNode);
            }
        }
        nextNodes = uniqeByKeys(nextNodes,['nodeId']);

        return nextNodes;
    }

    var level = 1;
    var nodes = [];
    nodes.push(_rootNode);
    while(nodes.length){
        var nextNodes = [];
        for(var i = 0; i < nodes.length; i++){
            var node = nodes[i];
            node.layout.level = level;
            nextNodes = nextNodes.concat(getNextNodes(node.nodeId,svg_links,level));
        }
        level++;
        nodes = nextNodes;
    }
}
// 数据处理：设置节点角色
function setCategoryColor(nodes, links){
    for(var i = 0; i < links.length; i++){
        var sameLink = {}; // 两点间连线信息
        sameLink.length = 0; // 两点间连线数量
        sameLink.currentIndex = 0; // 当前线索引
        sameLink.isSetedSameLink = false;
        links[i].sameLink = sameLink;
    }

    /*链接相同两点的线*/
    for(var i = 0; i < links.length; i++){
        var baseLink = links[i];

        if(baseLink.sameLink.isSetedSameLink == false){
            baseLink.sameLink.isSetedSameLink = true;
            var nodeId1 = baseLink.sourceNode.nodeId;
            var nodeId2 = baseLink.targetNode.nodeId;

            var sameLinks = [];
            sameLinks.push(baseLink);
            for(var j = 0; j < links.length; j++){
                var otherLink = links[j];
                if(baseLink.linkId != otherLink.linkId && !otherLink.sameLink.isSetedSameLink){
                    if((otherLink.sourceNode.nodeId == nodeId1 && otherLink.targetNode.nodeId == nodeId2 ) ||
                        (otherLink.sourceNode.nodeId == nodeId2 && otherLink.targetNode.nodeId == nodeId1 ) ){
                        sameLinks.push(otherLink);
                        otherLink.sameLink.isSetedSameLink = true;
                    }
                }
            }

            for(var k = 0; k < sameLinks.length; k++){
                var oneLink = sameLinks[k];
                oneLink.sameLink.length = sameLinks.length; // 两点间连线数量
                oneLink.sameLink.currentIndex = k; // 当前线索引
            }
        }
    }

    for(var i = 0; i < nodes.length; i++) {
        var node = nodes[i];
        // add by chenez for 通过公司名称判断是否当前节点 at 2018/9/25 16:16 start

        // add by chenez for 通过公司名称判断是否当前节点 at 2018/9/25 16:16 end

        /*if (_currentKeyNo == node.data.obj.properties.keyNo) {*/     /*keyNo字段取消*/
        if (_currentName == node.data.obj.properties.name){// 当前节点
            console.log(node)
            node.data.color = _COLOR.node.current;
            node.data.strokeColor = _COLOR.border.current;
        } else if (node.data.obj.labels[0] == 'Company') {
            node.data.color = _COLOR.node.company;
            node.data.strokeColor = _COLOR.border.company;
        } else {
            node.data.color = _COLOR.node.person;
            node.data.strokeColor = _COLOR.border.person;
        }
    }
}
// 数据处理：设置唯一孩子
function setSingleLinkNodes(links){
    function isSingleLink (nodeId,links){
        var hasLinks = 0;
        var isSingle = true;
        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if(link.targetNode.nodeId == nodeId || link.sourceNode.nodeId == nodeId){
                hasLinks++;
            }
            if(hasLinks > 1){
                isSingle = false;
                break;
            }
        }

        return isSingle;
    } // isSingleLink

    links.forEach(function (link,i) {
        if(isSingleLink(link.sourceNode.nodeId,links)){
            link.targetNode.layout.singleLinkChildren.push(link.sourceNode);
        }
        if(isSingleLink(link.targetNode.nodeId,links)){
            link.sourceNode.layout.singleLinkChildren.push(link.targetNode);
        }
    });
}
// 数据处理：根据nodeId获取node 索引
function getNodesIndex(nodeId,nodes) {
    var index = 0;
    for(var i = 0; i < nodes.length; i++){
        var node = nodes[i];
        if(nodeId == node.nodeId){
            index = i;
            break;
        }
    }
    return index;
}
// 数据处理：node是否存在
function isNodeExist(nodeId,nodes) {
    var exist = false;
    for(var i = 0; i < nodes.length; i++){
        var node = nodes[i];
        if(nodeId == node.nodeId){
            exist = true;
            break;
        }
    }
    return exist;
}
// 数据处理：根据nodes过滤出相应连线（没有节点的连线删除）
function filterLinksByNodes(nodes,allLinks) {
    function isExists(nodes,nodeId) {
        var exist = false;
        for(var i = 0; i < nodes.length; i++){
            var node = nodes[i];
            if(node.nodeId == nodeId){
                exist = true;
                break;
            }
        }
        return exist;
    }
    var sel_links = [];
    for(var i = 0; i < allLinks.length; i++){
        var link = allLinks[i];
        if(isExists(nodes,link.sourceNode.nodeId) && isExists(nodes,link.targetNode.nodeId)){
            //link.source = getNodesIndex(link.sourceNode.nodeId,nodes);
            //link.target = getNodesIndex(link.targetNode.nodeId,nodes);
            sel_links.push(link);
        }
    }
    return sel_links;
}
// 数据处理：根据links过滤出相应节点(没有连线的节点删除)
function filterNodesByLinks(nodes,links) {
    function isExists(links,nodeId) {
        var exist = false;
        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if(link.sourceNode.nodeId == nodeId || link.targetNode.nodeId == nodeId){
                exist = true;
                break;
            }
        }
        return exist;
    }
    var sel_nodes = [];
    for(var i = 0; i < nodes.length; i++){
        var node = nodes[i];
        if(isExists(links,node.nodeId)){
            sel_nodes.push(node);
        }
    }
    return sel_nodes;
}
// 数据处理：根据nodeId获取node
function getGraphNode(nodeId,nodes) {
    var node = null;
    for(var i = 0; i < nodes.length; i++){
        if(nodes[i].nodeId == nodeId) {
            node = nodes[i];
            break;
        }
    }
    return node;
}
// 数据处理：获取子节点
function getSubNodes(node,links) {
    var subNodes = [];
    var nodeId = node.nodeId;
    var level = node.layout.level;
    for(var i = 0; i < links.length; i++){
        var link = links[i];
        if(link.sourceNode.nodeId == nodeId && link.targetNode.layout.level == level+1){
            subNodes.push(link.targetNode);
        }
        if(link.targetNode.nodeId == nodeId && link.sourceNode.layout.level == level+1){
            subNodes.push(link.sourceNode);
        }
    }
    subNodes = uniqeByKeys(subNodes,['nodeId']);
    return subNodes;
}

/**筛选*/
// 数据处理：按状态过滤
function filterNodesByLevel(level,nodes){
    var sel_nodes = [];
    nodes.forEach(function (node) {
        if(node.layout.level <= level){
            sel_nodes.push(node);
        }
    })
    return sel_nodes;
}
// 数据处理：按状态过滤
function filterNodesByStatus(status,nodes){
    if(status == 'all'){
        return nodes;
    }

    var sel_nodes = [];
    for(var i = 0; i < nodes.length; i++){
        var node = nodes[i];
        if((node.data.obj.labels == 'Company' && node.data.obj.properties.status == status) || node.nodeId == _rootNode.nodeId){
            sel_nodes.push(node);
        }
    }
    return sel_nodes;
}
// 数据处理：按持股数过滤
function filterNodesByStockNum(num,links){
    var sel_links = [];
    for(var i = 0; i < links.length; i++){
        if(num == links[i].data.obj.properties.stockPercent){
            sel_links.push(links[i]);
        }
    }
    return sel_links;
}
// 数据处理：按投资过滤
function filterNodesByInvest(invest,nodes,links){
    console.log(invest)
    /*获取直接投资的节点*/
    function getInvestNodes(nodeId,links) {
        var investNodes = [];
        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if(link.sourceNode.nodeId == nodeId && link.data.obj.type == 'INVEST'){
                investNodes.push(link.targetNode);
            }
        }

        //investNodes = uniqeByKeys(investNodes,['nodeId']);
        return investNodes;
    }
    /*获取公司股东*/
    function getCompanyStockholder(nodeId,links) {
        var stockholderNodes = [];
        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if(link.targetNode.nodeId == nodeId && link.data.obj.type == 'INVEST'){
                stockholderNodes.push(link.sourceNode);
            }
        }

        //stockholderNodes = uniqeByKeys(stockholderNodes,['nodeId']);
        return stockholderNodes;
    }
    /*获取董监高法*/
    function getPersonStockholder(nodeId,links) {
        var stockholderNodes = [];
        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if(link.targetNode.nodeId == nodeId && link.data.obj.type == 'INVEST' && link.sourceNode.data.obj.labels[0] == 'Person'){
                stockholderNodes.push(link.sourceNode);
            }
        }

        //stockholderNodes = uniqeByKeys(stockholderNodes,['nodeId']);
        return stockholderNodes;
    }

    var sel_nodes = [];

    switch (invest){
        case 'all':
            return nodes;
            break;
        case 'direct': //直接投资
            sel_nodes = getInvestNodes(_rootNode.nodeId,links);
            break;
        case 'stockholder': // 股东投资
            var nextNodes = [];
            var stockholderNodes = getCompanyStockholder(_rootNode.nodeId,links);
            for(var i = 0; i < stockholderNodes.length; i++){
                nextNodes = nextNodes.concat(getInvestNodes(stockholderNodes[i].nodeId,links));
            }
            sel_nodes = stockholderNodes.concat(nextNodes);
            break;
        case 'legal': // 董监高法投资
            var nextNodes = [];
            var stockholderNodes = getPersonStockholder(_rootNode.nodeId,links);
            for(var i = 0; i < stockholderNodes.length; i++){
                nextNodes = nextNodes.concat(getInvestNodes(stockholderNodes[i].nodeId,links));
            }
            sel_nodes = stockholderNodes.concat(nextNodes);
            break;
    }


    sel_nodes = sel_nodes.concat(_rootNode);
    sel_nodes = uniqeByKeys(sel_nodes,['nodeId']);
    return sel_nodes;
}
// 数据处理：根据所有条件过滤
function filter(rootData){
    function isParentExist(node,nodes,links) {
        var isExist = false;
        var parentLevel = node.layout.level - 1;

        if(parentLevel < 2){
            return true;
        }

        for(var i = 0; i < links.length; i++){
            var link = links[i];
            if((link.sourceNode.nodeId == node.nodeId && link.targetNode.layout.level == parentLevel) && isNodeExist(link.targetNode.nodeId,nodes) ){
                isExist = true;
                break;
            }
            if((link.targetNode.nodeId == node.nodeId && link.sourceNode.layout.level == parentLevel) && isNodeExist(link.sourceNode.nodeId,nodes) ){
                isExist = true;
                break;
            }
        }

        return isExist;
    }
    function getFilterData(rootData) {
        //
        var sel_nodes = [];
        for(var i = 0; i < rootData.nodes.length; i++){
            sel_nodes.push(rootData.nodes[i]);
        }
        var sel_links = [];
        for(var i = 0; i < rootData.links.length; i++){
            sel_links.push(rootData.links[i]);
        }

        var level = $('#SelPanel').attr('param-level');
        var status = $('#SelPanel').attr('param-status');
        var num = $('#SelPanel').attr('param-num');
        var invest = $('#SelPanel').attr('param-invest');

        //console.log('status:' + status + ' num:' + num + ' invest:' + invest);

        // 层级
        level = parseInt(level) + 1;
        sel_nodes = filterNodesByLevel(level,sel_nodes);

        // 状态
        if(status){
            sel_nodes = filterNodesByStatus(status,sel_nodes);
        }

        // 持股
        var stock_nodes = [];
        if(num && num != 0){
            sel_links = filterLinksByNodes(sel_nodes,sel_links);
            sel_links = filterNodesByStockNum(num,sel_links);
            for(var i = 0; i < sel_links.length;i++){
                stock_nodes.push(sel_links[i].sourceNode);
                stock_nodes.push(sel_links[i].targetNode);
            }
            sel_nodes = uniqeByKeys(stock_nodes,['nodeId']);
        }

        // 投资
        if(invest){
            sel_nodes = filterNodesByInvest(invest,sel_nodes,sel_links);
        }

        //父节点不存在则删除
        var sel_nodes2 = [];
        sel_nodes.forEach(function (node,i) {
            if(isParentExist(node,sel_nodes,sel_links)){
                sel_nodes2.push(node);
            }
        })
        sel_links = filterLinksByNodes(sel_nodes2,sel_links);

        return {links:sel_links,nodes:sel_nodes2};
    }

    var nodesIds = [];
    var selGraph= getFilterData(rootData)
    selGraph.nodes.forEach(function (node) {
        nodesIds.push(node.nodeId);
    })
    highLightFilter(nodesIds,cy);

    /*//
    //$("#load_data").show();

    // 保证始终存在当前节点
    /!*if(sel_nodes2.length == 0){
        sel_nodes2.push(_rootNode);
    }*!/

    // 更新图谱
    /!*$("#TrTxt").removeClass('active');
    domUpdate(getFilterData(rootData));*!/

    /!*setTimeout(function () {
        domUpdate({links:sel_links,nodes:sel_nodes2});
    },5000)*!/;*/
}
//
function filterReset(){
    $('#SelPanel').attr('param-level','2');
    $('#SelPanel').attr('param-status','');
    $('#SelPanel').attr('param-num','');
    $('#SelPanel').attr('param-invest','');

    $('#ShowLevel a').removeClass('active');
    $('#ShowLevel a').eq(1).addClass('active');
    $('#ShowStatus a').removeClass('active');
    $('#ShowStatus a').eq(0).addClass('active');
    $('#ShowInvest a').removeClass('active');
    $('#ShowInvest a').eq(0).addClass('active');
    $('#inputRange').val(0);
    $('#inputRange').css({'backgroundSize':'0% 100%'});
}

/****** Html 相关 ******/
// 水印居中
function printLogoFixed() {
    var bodyH = $('body').height();
    var imgH = $('.printLogo img').height();
    var top = (parseFloat(bodyH) - parseFloat(imgH)) / 2;
    $('.printLogo img').css({'margin-top' : top + 'px'});
}
//筛选面板：显示
function selPanelShow() {
    $('.tp-sel').fadeIn();
    //$('.tp-sel').addClass('zoomIn');
    $('#TrSel').addClass('active');
}
//筛选面板：隐藏
function selPanelHide() {
    $('.tp-sel').fadeOut();
    $('#TrSel').removeClass('active');
}
//筛选面板：列表更新
function selPanelUpdateList(nodes,links,isShowCheckbox) {
    $('.tp-list').html('');
    for(var i = 0; i < nodes.length; i++){
        var node = nodes[i];
        var index = i + 1;
        var name = node.data.obj.properties.name;
        /*var keyNo = node.data.obj.properties.keyNo;*/  /*keyNo字段取消*/
        var str = '';
        if(isShowCheckbox){   /*keyNo字段取消*/
            str = '<div class="checkbox" node_id="'+ node.nodeId +'" name="'+ name +'"> <input checked type="checkbox"><label> ' + index + '.' + name + '</label> </div>';
//            var str = '<div class="checkbox" node_id="'+ node.nodeId +'" keyno="'+ keyNo +'"> <label> ' + index + '.' + name + '</label> </div>';
        } else {
            str = '<div class="checkbox" node_id="'+ node.nodeId +'" name="'+ name +'"><label> ' + index + '.' + name + '</label> </div>';
        }

        $('.tp-list').append(str);
    }

    $('.tp-list > div > label').click(function () {
        var _parent = $(this).parent();
        var nodeId = _parent.attr('node_id');

        focusReady(getGraphNode(nodeId,nodes));
    });

    $('.tp-list > div > input').click(function () {
        /*var _this = $(this);
        var _parent = $(this).parent();
        var nodeId = _parent.attr('node_id');
        var checkedNodeIds = $('.tp-list').attr('node_ids');
        if(checkedNodeIds){
            checkedNodeIds = checkedNodeIds.split(',');
        }*/

        var checkedNodeIds = [];
        $('.tp-list input:checked').each(function () {
            var _parent = $(this).parent();
            var nodeId = _parent.attr('node_id');
            checkedNodeIds.push(nodeId);
        });

        /*if(_this.is(':checked')){
            checkedNodeIds.push(nodeId);
            nodes.splice(1,1);
            console.log('checked');
        } else {
            console.log('un checked');
            var sub_nodes = []
            sub_nodes = nodes.splice(0,1);
            console.log(nodes);
            console.log(sub_nodes);
            graphInit(nodes, links);
        }*/
        highLight(checkedNodeIds,cy);
        /*// 需要隐藏的节点及子节点
        var choosedNode = getGraphNode(nodeId,nodes);
        var subNodes = getSubNodes(choosedNode,links);
        subNodes.push(choosedNode);

        // 剩下的节点
        var lastNodes = [];
        for(var i = 0; i < nodes.length; i++){
            var node = nodes[i];
            if(!getGraphNode(node.nodeId,subNodes)){
                lastNodes.push(node);
            }
        }

        // 剩下的连线
        var lastLinks = filterLinksByNodes(lastNodes,links);

        graphInit(lastNodes, lastLinks);
        if(_this.is(':checked')){
            nodes.splice(1,1);
            console.log('checked');
        } else {
            console.log('un checked');
            var sub_nodes = []
            sub_nodes = nodes.splice(0,1);
            console.log(nodes);
            console.log(sub_nodes);
            graphInit(nodes, links);
        }
        console.log(nodeId);*/
    });
}
//筛选面板：聚焦准备
function focusReady(node) {
    filterReset();
    $('#FocusInput').val(node.data.obj.properties.name);
    $('#FocusInput').attr('node_id',node.nodeId);
    $('#FocusBt').text('聚焦');
    $('#FocusBt').removeClass('focusDisable');
    $('#ClearInput').show();
}
//筛选面板：取消聚焦
function focusCancel() {
    $('#ClearInput').hide();
    $('#FocusBt').text('聚焦');
    $('#FocusBt').addClass('focusDisable');
    $('#FocusInput').val('');
    $('#FocusInput').attr('node_id','');
    selPanelUpdateList(_rootData.nodes,_rootData.links,true);
    cancelHighLight();
}

function maoScale(type){

    /*var c=$('canvas').eq(2).attr('id','myCanvas');
    var c=document.getElementById("myCanvas");
    console.log(c);
    var ctx = c.getContext("2d");
    ctx.font = "5px Arial";
    ctx.fillText("上海", 1, 10);

    return;*/

    //
    var rate = 0.2;
    var scale = cy.zoom();
    if(type==1){
        scale += rate;
    }else if(type==2){
        scale -= rate;
    }

    cy.zoom({
        level: scale, // the zoom level
    });
}
//全屏/退出切换
/*function resizeScreen(){
    if(isFullScreen()){
        $('#TrFullScreen').addClass('active');
        $('#TrFullScreen').html('<span class="screen2ed"></span>退出');
    } else {
        $('#TrFullScreen').removeClass('active');
        $('#TrFullScreen').html('<span class="screen2"></span>全屏');
    }

    //cy.pan();
    /!*if(document.body.clientHeight>700){
        $('#Main').height(document.body.clientHeight-66);
        console.log(document.body.clientHeight);
    }else{
        $('#Main').height(640);
    }*!/
}*/

function isFullScreen(){
    if(document.fullscreen){
        return true;
    }else if(document.mozFullScreen){
        return true;
    }else if(document.webkitIsFullScreen){
        return true;
    }else if(document.msFullscreenElement){
        return true;
    }else{
        return false;
    }
}
//全屏打开
function launchFullScreen(element) {

    if(element.requestFullscreen) {
        element.requestFullscreen();
    }else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    }else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}
//退出全屏
function exitFullScreen(){

    if(document.exitFullscreen){
        document.exitFullscreen();
    }
    else if(document.mozCancelFullScreen){
        document.mozCancelFullScreen();
    }
    else if(document.msExitFullscreen){
        document.msExitFullscreen();
    }
    else if(document.webkitCancelFullScreen){
        document.webkitCancelFullScreen();
    }
}

//切换是否显示线条上的文字
function toggleText() {
    if($("#TrTxt").hasClass('active')){
        $("#TrTxt").removeClass('active');
        cy.collection("edge").removeClass("edgeShowText");
    } else {
        $("#TrTxt").addClass('active');
        cy.collection("edge").addClass("edgeShowText");
    }
}

/****** 图谱 相关 ******/
/*function highlight( node ){
    var oldNhood = lastHighlighted;

    var nhood = lastHighlighted = node.closedNeighborhood();
    var others = lastUnhighlighted = cy.elements().not( nhood );

    var reset = function(){
        cy.batch(function(){
            others.addClass('hidden');
            nhood.removeClass('hidden');

            allEles.removeClass('faded highlighted');

            nhood.addClass('highlighted');

            others.nodes().forEach(function(n){
                var p = n.data('orgPos');

                n.position({ x: p.x, y: p.y });
            });
        });

        return Promise.resolve().then(function(){
            if( isDirty() ){
                return fit();
            } else {
                return Promise.resolve();
            };
        }).then(function(){
            return Promise.delay( aniDur );
        });
    };

    var runLayout = function(){
        var p = node.data('orgPos');

        var l = nhood.filter(':visible').makeLayout({
            name: 'concentric',
            fit: false,
            animate: true,
            animationDuration: aniDur,
            animationEasing: easing,
            boundingBox: {
                x1: p.x - 1,
                x2: p.x + 1,
                y1: p.y - 1,
                y2: p.y + 1
            },
            avoidOverlap: true,
            concentric: function( ele ){
                if( ele.same( node ) ){
                    return 2;
                } else {
                    return 1;
                }
            },
            levelWidth: function(){ return 1; },
            padding: layoutPadding
        });

        var promise = cy.promiseOn('layoutstop');

        l.run();

        return promise;
    };

    var fit = function(){
        return cy.animation({
            fit: {
                eles: nhood.filter(':visible'),
                padding: layoutPadding
            },
            easing: easing,
            duration: aniDur
        }).play().promise();
    };

    var showOthersFaded = function(){
        return Promise.delay( 250 ).then(function(){
            cy.batch(function(){
                others.removeClass('hidden').addClass('faded');
            });
        });
    };

    return Promise.resolve()
        .then( reset )
        .then( runLayout )
        .then( fit )
        .then( showOthersFaded )
        ;

}//hilight*/
function drawGraph(elements) {
    _currentKeyNo,_companyRadius = 35,_personRadius = 15,_circleMargin = 10,_circleBorder = 3,_currentName;
    cy = cytoscape({
        container: document.getElementById('MainCy'),
        motionBlur: false,
        textureOnViewport:false,
        wheelSensitivity:0.1,
        elements:elements,
        minZoom:0.4,
        maxZoom:2.5,
        layout: {
            name: 'preset',
            componentSpacing: 40,
            nestingFactor:12,
            padding: 10,
            edgeElasticity:800,
            stop:function (e) {

                //解决浏览器标签切换排列问题
                if(document[state] == 'hidden'){
                    _isNeedReload = true;
//                        console.log('stop _isNeedReload=true');
                } else {
                    _isNeedReload = false;
                }
                setTimeout(function () {
                    if(document[state] == 'hidden'){
                        _isGraphLoaded = false;
                        console.log('stop _isGraphLoaded=false');
                    } else {
                        _isGraphLoaded = true;
                    }
                },1000);
            }
        },
        style: [
            {
                selector: 'node',
                style: {
                    shape: 'ellipse',
                    width: function (ele) {
                        //当前节点有图片
                        /*if(ele.data("type") == 'Person' && _currentKeyNo == ele.data('keyNo') && ele.data('hasImage')){*/ /*keyNo字段取消*/
                        if (ele.data("type") == 'Person' && _currentName == ele.data('name')){
                            return 80;
                        }
                        //有图片
                        /*if(ele.data('hasImage') && ele.data('type') == 'Person'){
                            return 60;
                        }*/
                        //普通
                        if(ele.data("type") == 'Company'){
                            return 60;
                        }
                        return 45;
                    },
                    height: function (ele) {
                        //当前节点有图片
                       /* if(ele.data("type") == 'Person' && _currentKeyNo == ele.data('keyNo') && ele.data('hasImage')){*/  /*keyNo字段取消*/
                        if (ele.data('type')=='Person' && _currentName == ele.data('name')){
                            return 80;
                        }
                        /*//有图片
                        if(ele.data('hasImage') && ele.data('type') == 'Person'){
                            return 60;
                        }*/
                        //普通
                        if(ele.data("type") == 'Company'){
                            return 60;
                        }
                        return 45;
                    },
                    'background-color': function (ele) {
                        return ele.data('color');
                    },
                    'background-fit': 'cover',
                    'background-image': function (ele) {
                        /*var hasImage = ele.data('hasImage');*/
                        /*var keyNo = ele.data('keyNo');*/   /*keyNo字段取消*/
                        var name = ele.data('name');
                        var type = ele.data('type');
                        /*if(hasImage && type == 'Person'){
                            return '/proxyimg_'+ name+'.jeg';
                        } else {
                            return 'none';
                        }*/
                        return 'none';
                    },
                    // 'background-image-crossorigin': 'use-credentials',
                    'border-color': function (ele) {
                        return ele.data("borderColor");
                    },
                    'border-width': function (ele) {
                        /*if(ele.data('hasImage') && ele.data('type') == 'Person'){
                            return 3;
                        } else {
                            return 1;
                        }*/
                        return 1;
                    },
                    'border-opacity': 1,
                    label: function (ele) {
                        var label = ele.data("name");
                        var length = label.length;

                        if(length <=5){ // 4 5 4排列
                            return label;
                        } else if(length >=5 && length <= 9) {
                            return label.substring(0,length - 5) + '\n' + label.substring(length - 5,length);
                        } else if(length >= 9 && length <= 13){
                            return label.substring(0,4) + '\n' + label.substring(4,9) + '\n' + label.substring(9,13);
                        } else {
                            return label.substring(0,4) + '\n' + label.substring(4,9) + '\n' + label.substring(9,12) + '..';
                        }
                    },
                    'z-index-compare':'manual',
                    'z-index':20,
                    color:"#fff",
                    //'padding-top':0,
                    'padding':function (ele) {
                        if(ele.data("type") == 'Company'){
                            return 3;
                        }
                        return 0;
                    },
                    'font-size':12,
                    //'min-height':'400px',
                    //'ghost':'yes',
                    //'ghost-offset-x':300,
                    //'font-weight':800,
                    //'min-zoomed-font-size':6,
                    'font-family':'microsoft yahei',
                    'text-wrap':'wrap',
                    'text-max-width':60,
                    'text-halign':'center',
                    'text-valign':'center',
                    'overlay-color':'#fff',
                    'overlay-opacity':0,
                    'background-opacity':1,
                    'text-background-color':'#000',
                    'text-background-shape':'roundrectangle',
                    'text-background-opacity':function (ele) {
                        /*if(ele.data('hasImage') && ele.data('type') == 'Person'){
                            return 0.3;
                        } else {
                            return 0
                        }*/
                        return 0;
                    },
                    'text-background-padding':0,
                    'text-margin-y': function (ele) {
                        //当前节点有图片
                        /*if(ele.data("type") == 'Person' && _currentKeyNo == ele.data('keyNo') && ele.data('hasImage')){*/ /*keyNo字段取消*/
                        if (ele.data("type") == 'Person' && _currentName == ele.data('name')){
                            return 23;
                        }
                        // 有图片
                        /*if(ele.data('hasImage') && ele.data('type') == 'Person'){
                            return 16;
                        }*/
                        //
                        if(ele.data("type") == 'Company'){
                            return 4;
                        }
                        return 2;
                    },
                },
            },
            {
                selector: 'edge',
                style: {
                    'line-style':function (ele) {
                        return 'solid';
                        /*if(ele.data('data').obj.type == 'INVEST'){
                            return 'solid';
                        } else {
                            return 'dashed'
                        }*/
                    },
                    'curve-style': 'bezier',
                    'control-point-step-size':20,
                    'target-arrow-shape': 'triangle-backcurve',
                    'target-arrow-color': function (ele) {
                        return ele.data("color");
                    },
                    'arrow-scale':0.5,
                    'line-color': function (ele) {
                        //return '#aaaaaa';
                        return ele.data("color");
                    },
                    label: function (ele) {
                        return '';
                    },
                    'text-opacity':0.8,
                    'font-size':12,
                    'background-color':function (ele) {
                        return '#ccc';
                        return ele.data("color");
                    },
                    'width': 0.3,
                    'overlay-color':'#fff',
                    'overlay-opacity':0,
                    'font-family':'microsoft yahei',
                }
            },
            {
                "selector": ".autorotate",
                "style": {
                    "edge-text-rotation": "autorotate"
                }
            },
            {
                selector:'.nodeActive',
                style:{
                    /*'background-color':function (ele) {
                        if(ele.data("category")==1){
                            return "#5c8ce4"
                        }
                        return "#d97a3a";
                    },*/
                    //'z-index':300,
                    'border-color': function (ele) {
                        return ele.data("color");
                    },
                    'border-width': 10,
                    'border-opacity': 0.5
                }
            },
            {
                selector:'.edgeShow',
                style:{
                    'color':'#999',
                    'text-opacity':1,
                    'font-weight':400,
                    label: function (ele) {
                        return ele.data("label");
                    },
                    'font-size':10,
                }
            },
            {
                selector:'.edgeActive',
                style:{
                    'arrow-scale':0.8,
                    'width': 1.5,
                    'color':'#330',
                    'text-opacity':1,
                    'font-size':12,
                    'text-background-color':'#fff',
                    'text-background-opacity':0.8,
                    'text-background-padding':0,
                    'source-text-margin-y':20,
                    'target-text-margin-y':20,
                    //'text-margin-y':3,
                    'z-index-compare':'manual',
                    'z-index':1,
                    'line-color': function (ele) {
                        return ele.data("color");
                    },
                    'target-arrow-color': function (ele) {
                        return ele.data("color");
                    },
                    label: function (ele) {

                        /*if(ele.data('data').obj.type == 'INVEST'){
                            return 'solid';
                        } else {
                            return 'dashed'
                        }*/
                        return ele.data("label");
                    }
                }

            },
            {
                selector:'.hidetext',
                style:{
                    'text-opacity':0,
                }
            },
            {
                selector:'.dull',
                style:{
                    'z-index':1,
                    opacity:0.2,
                }
            },
            {
                selector: '.nodeHover',
                style: {
                    shape: 'ellipse',
                    'background-opacity':0.9,
                }
            },
            {
                selector: '.edgeLevel1',
                style: {
                    label: function (ele) {
                        return ele.data("label");
                    },
                }
            },
            {
                selector: '.edgeShowText',
                style: {
                    label: function (ele) {
                        return ele.data("label");
                    },
                }
            },
            {
                selector: '.lineFixed',// 加载完成后，加载该类，修复线有锯齿的问题
                style: {
                    'overlay-opacity':0,
                }
            },
        ],
    });
    //点击事件
    cy.on('click', 'node', function(evt){
        if(evt.target._private.style['z-index'].value == 20) { // 非暗淡状态
            _isFocus = true;
            var node = evt.target;

            highLight([node._private.data.id],cy);

            if(node.hasClass("nodeActive")){
                activeNode = null;
                $('#company-detail').hide();
                node.removeClass("nodeActive");
                cy.collection("edge").removeClass("edgeActive");
            }else{
                var nodeData = node._private.data;
                if(nodeData.type == 'Company'){
                    showDetail2(nodeData.name,'company_muhou3');    /*keyNo字段取消*/
                    cy.collection("node").addClass('nodeDull');
                } else {
                    showDetail2(nodeData.name,'company_muhou3','person');  /*keyNo字段取消*/
                    cy.collection("node").addClass('nodeDull');
                }

                activeNode = node;
                cy.collection("node").removeClass("nodeActive");

                cy.collection("edge").removeClass("edgeActive");
                node.addClass("nodeActive");
                node.neighborhood("edge").removeClass("opacity");
                node.neighborhood("edge").addClass("edgeActive");
                node.neighborhood("edge").connectedNodes().removeClass("opacity");
            }
            //_firstTab = false;
        } else {
            _isFocus = false;
            activeNode = null;
            cy.collection("node").removeClass("nodeActive");
            $('.tp-detail').fadeOut();
            cancelHighLight();
        }
    });
    var showTipsTime = null;
    cy.on('mouseover', 'node', function(evt){
        if(evt.target._private.style['z-index'].value == 20){ // 非暗淡状态
            //
            $("#Main").css("cursor","pointer");

            //
            var node = evt.target;
            node.addClass('nodeHover');
            if(!_isFocus){
                cy.collection("edge").removeClass("edgeShow");
                cy.collection("edge").removeClass("edgeActive");
                node.neighborhood("edge").addClass("edgeActive");
            }

            // 提示
            clearTimeout(showTipsTime);
            //if(node._private.data.name.length > 13 || (node._private.data.keyNo[0] == 'p' && node._private.data.name.length > 3) || node._private.data.layout.revel > 2){
            if(node._private.data.name.length > 13 ||  node._private.data.name.length > 3){
                showTipsTime = setTimeout(function () {
                    var name = node._private.data.name;


                    // 显示在节点位置
                    /*var tipWidth = name.length * 12 + 16;
                    var x = node._private.data.d3x + 655 - (tipWidth / 2);
                    var y = node._private.data.d3y + 598;
                    if(node._private.data.type == 'Person'){
                        y = node._private.data.d3y + 590;
                    }*/


                    // 显示在鼠标位置
                    var event = evt.originalEvent ||window.event;
                    var x = event.clientX + 10;
                    var y = event.clientY + 10;

                    var html = "<div class='tips' style='font-size:12px;background:white;box-shadow:0px 0px 3px #999;border-radius:1px;opacity:1;padding:1px;padding-left:8px;padding-right:8px;display:none;position: absolute;left:"+ x +"px;top:"+ y +"px;'>"+ name +"</div>";
                    $('body').append($(html));
                    $('.tips').fadeIn();
                },600);
            }
        }
    });
    cy.on('mouseout', 'node', function(evt){
        $("#Main").css("cursor","default");

        // 提示
        $('.tips').fadeOut(function () {
            $('.tips').remove();
        });

        clearTimeout(showTipsTime);

        //
        var node = evt.target;
        node.removeClass('nodeHover');
        if(!_isFocus){
            cy.collection("edge").removeClass("edgeActive");
            /*if(moveTimeer){
                clearTimeout(moveTimeer);
            }*/
            /*moveTimeer = setTimeout(function() {
                cy.collection("edge").addClass("edgeActive");
                //cy.collection("edge").addClass("edgeShow");
            }, 300);
            if(activeNode){
                activeNode.neighborhood("edge").addClass("edgeActive");
            }*/
        }
    });
    cy.on('mouseover', 'edge', function(evt){
        if(!_isFocus){
            var edge = evt.target;
            /*if(moveTimeer){
                clearTimeout(moveTimeer);
            }*/
            cy.collection("edge").removeClass("edgeActive");
            edge.addClass("edgeActive");
            /*if(activeNode){
                activeNode.neighborhood("edge").addClass("edgeActive");
            }*/
        }

    });
    cy.on('mouseout', 'edge', function(evt){
        if(!_isFocus){
            var edge = evt.target;
            edge.removeClass("edgeActive");
            // moveTimeer = setTimeout(function() {
            //     cy.collection("edge").addClass("edgeActive");
            //     //cy.collection("edge").addClass("edgeShow");
            // }, 400);
            if(activeNode){
                activeNode.neighborhood("edge").addClass("edgeActive");
            }
        }

    });
    cy.on('vmousedown', 'node', function(evt){
        var node = evt.target;
        if(!_isFocus){
            highLight([node._private.data.id],cy);
        }
    });
    cy.on('tapend', 'node', function(evt){
        if(!_isFocus){
            cancelHighLight();
        }
    });

    cy.on('click', 'edge', function(evt){
        _isFocus = false;
        activeNode = null;
        cy.collection("node").removeClass("nodeActive");
        $('.tp-detail').fadeOut();
        cancelHighLight();
    });
    cy.on('click', function(event){
        var evtTarget = event.target;

        if( evtTarget === cy ){
            _isFocus = false;
            activeNode = null;
            cy.collection("node").removeClass("nodeActive");
            $('.tp-detail').fadeOut();
            cancelHighLight();
            focusCancel();
            filterReset();

            //cy.collection("edge").addClass("edgeActive");
        } else {
            //console.log('tap on some element');
        }
    });

    cy.on('zoom',function(){
        if(cy.zoom()<0.5){
            cy.collection("node").addClass("hidetext");
            cy.collection("edge").addClass("hidetext");
        }else{
            cy.collection("node").removeClass("hidetext");
            cy.collection("edge").removeClass("hidetext");
        }

        // 加载完成后，加载该类，修复线有锯齿的问题
        setTimeout(function () {
            cy.collection("edge").removeClass("lineFixed");
            cy.collection("edge").addClass("lineFixed");
        },200);
    })

    cy.on('pan',function () {
        // 加载完成后，加载该类，修复线有锯齿的问题
        setTimeout(function () {
            cy.collection("edge").removeClass("lineFixed");
            cy.collection("edge").addClass("lineFixed");
        },200);
    });

    // 定位
    cy.nodes().positions(function( node, i ){
        // 保持居中
        /*if(node._private.data.keyNo == _currentKeyNo){*/ /*keyNo字段取消*/
            if (node._private.data.name == _currentName){

            var position= cy.pan();
            cy.pan({
                x: position.x-node._private.data.d3x,
                y: position.y-node._private.data.d3y
            });
        }

        //
        return {
            x: node._private.data.d3x,
            y: node._private.data.d3y
        };
    });

    cy.ready(function () {


        if(!$('#TrTxt').hasClass('active')){
            $('#TrTxt').click();
        }

        cy.zoom({
            level: 1.0000095043745896, // the zoom level
        });
        $("#load_data").hide();
        //cy.$('#'+id).emit('tap');
        //cy.center(cy.$('#'+id));
        //cy.collection("edge").addClass("edgeActive");

        // 加载完成后，加载该类，修复线有锯齿的问题
        setTimeout(function () {
            cy.collection("edge").addClass("lineFixed");
        },400);

        // 首页的插入图谱默认高亮第一层
        if(_rootData && _rootData.nodes.length > 30 && typeof _INSERT_URL != 'undefined' && _INSERT_URL){
            highLight([_rootNode.nodeId],cy);
        }
    });

    cy.nodes(function (node) {

        /*
        // 当前查询节点关系文字显示
        if(node._private.data.nodeId == _rootNode.nodeId){
            node.neighborhood("edge").addClass("edgeLevel1");
        }*/
    });
}
function highLight(nodeIds,cy) {
    cy.collection("node").removeClass("nodeActive");
    cy.collection("edge").removeClass("edgeActive");
    cy.collection("node").addClass('dull');
    cy.collection("edge").addClass('dull');

    for(var i = 0; i < nodeIds.length; i++){
        var nodeId = nodeIds[i];
        cy.nodes(function (node) {
            var nodeData  = node._private.data;
            if(nodeData.id == nodeId){
                node.removeClass('dull');
                //node.addClass('nodeActive');
                node.neighborhood("edge").removeClass("dull");
                node.neighborhood("edge").addClass("edgeActive");
                node.neighborhood("edge").connectedNodes().removeClass("dull");
                //node.neighborhood("edge").connectedNodes().addClass("nodeActive");
            }
        });
    }
}
function highLightFilter(nodeIds,cy) {
    function isInNodeIds(nodeId) {
        for(var i = 0; i < nodeIds.length; i++){
            if(nodeId == nodeIds[i]){
                return true;
                break;
            }
        }
        return false;
    }

    cy.collection("node").removeClass("nodeActive");
    cy.collection("edge").removeClass("edgeActive");
    cy.collection("node").addClass('dull');
    cy.collection("edge").addClass('dull');


    for(var i = 0; i < nodeIds.length; i++){
        var nodeId = nodeIds[i];
        cy.nodes(function (node) {
            var nodeData  = node._private.data;
            if(nodeData.id == nodeId){
                node.removeClass('dull');
                //node.addClass('nodeActive');
                /*node.neighborhood("edge").removeClass("dull");
                node.neighborhood("edge").addClass("edgeActive");
                node.neighborhood("edge").connectedNodes().removeClass("dull");*/
                //node.neighborhood("edge").connectedNodes().addClass("nodeActive");
            }
        });
    }

    cy.edges(function (edge) {
        var data = edge._private.data;
        if(isInNodeIds(data.target) && isInNodeIds(data.source)){
            edge.removeClass('dull');
            edge.addClass('edgeActive');
        }
    });
}
function cancelHighLight() {
    cy.collection("node").removeClass("nodeActive");
    cy.collection("edge").removeClass("edgeActive");
    cy.collection("node").removeClass('dull');
    cy.collection("edge").removeClass('dull');
}

/**其他*/

function getD3Position(graph) {
    getLayoutNode(graph);

    function filterLinks1(graph) {
        // 筛选用于布局的links
        var layoutLinks = [];
        for(var i = 0; i < graph.links.length; i++){
            var link = graph.links[i];
            var sourceLevel = link.sourceNode.layout.level;
            var targetLevel = link.targetNode.layout.level;
            var sourceNode = link.sourceNode;
            var targetNode = link.targetNode;
//            sourceNode.layout.isSetLink = false;
//            targetNode.layout.isSetLink = false;


//            if(!sourceNode.layout.isSetLink && !targetNode.layout.isSetLink){
            if((sourceLevel == 1 && targetLevel == 2) || (sourceLevel == 2 && targetLevel == 1) ){
//                    sourceNode.layout.isSetLink = true;
//                    targetNode.layout.isSetLink = true;
                layoutLinks.push(link);
            }
            if((sourceLevel == 2 && targetLevel == 3) || (sourceLevel == 3 && targetLevel == 2) ){
//                    sourceNode.layout.isSetLink = true;
//                    targetNode.layout.isSetLink = true;
                layoutLinks.push(link);
            }
//            }

        }

        layoutLinks.forEach(function (link,i) {

            if(link.targetNode.layout.level == 3){
                layoutLinks.forEach(function (alink,j) {
                    if(alink.linkId != link.linkId &&
                        (alink.targetNode.nodeId == link.targetNode.nodeId || alink.sourceNode.nodeId == link.targetNode.nodeId)){
                        layoutLinks.splice(j,1);
                    }
                })
            }

            if(link.sourceNode.layout.level == 3){
                layoutLinks.forEach(function (alink,j) {
                    if(alink.linkId != link.linkId &&
                        (alink.targetNode.nodeId == link.sourceNode.nodeId || alink.sourceNode.nodeId == link.sourceNode.nodeId)){
                        layoutLinks.splice(j,1);
                    }
                })
            }
        })

        return layoutLinks;
    }

    function filterLinks2(graph) {
        // 筛选用于布局的links
        var layoutLinks = [];
        for(var i = 0; i < graph.links.length; i++){
            var link = graph.links[i];
            var sourceLevel = link.sourceNode.layout.level;
            var targetLevel = link.targetNode.layout.level;
            var sourceNode = link.sourceNode;
            var targetNode = link.targetNode;


            if((sourceLevel == 1 && targetLevel == 2) || (sourceLevel == 2 && targetLevel == 1) ){
                layoutLinks.push(link);
            }
            if((sourceLevel == 2 && targetLevel == 3) || (sourceLevel == 3 && targetLevel == 2) ){
                layoutLinks.push(link);
            }

        }

        return layoutLinks;
    }

    function initD3Data(graph) { //
        function getIndex(val,arr) {
            var index = 0;
            for(var i = 0; i < arr.length; i++){
                var obj = arr[i];
                if(val == obj.nodeId){
                    index = i;
                    break;
                }
            }
            return index;
        }

        /*封装符合d3的数据*/
        for(var i = 0; i < graph.nodes.length; i++){
            var node = graph.nodes[i];
            node.id = node.nodeId;
        }

        for(var i = 0; i < graph.links.length; i++){
            var link = graph.links[i];
            link.source = getIndex(link.sourceNode.nodeId, graph.nodes) ;
            link.target = getIndex(link.targetNode.nodeId, graph.nodes) ;
            link.index = i; //
        }

        graph.layoutLinks = filterLinks1(graph);

        // 围绕节点最大数值
        setSingleLinkNodes(graph.layoutLinks);
        graph.nodes.forEach(function(node,i){
            if(node.layout.singleLinkChildren.length && _maxChildrenLength < node.layout.singleLinkChildren.length){
                _maxChildrenLength = node.layout.singleLinkChildren.length
            }
        })
        //console.log('围绕节点最大数值:' + _maxChildrenLength);
    }

    initD3Data(graph); //

    var width = $("#MainD3 svg").width();
    var height = $("#MainD3 svg").height();

    var strength = -600,distanceMax = 330,theta = 0,distance = 130,colideRadius = 35,distanceMin = 400;
    // 根据节点数量调节
    if(graph.nodes.length < 50 ){
        strength = -800;distanceMax = 400;
    } else if( graph.nodes.length > 50 && graph.nodes.length < 100 ){
        strength = -800;distanceMax = 350;distance = 130;colideRadius = 35;
    } else if(graph.nodes.length > 100 && graph.nodes.length < 150){
        strength = -900;distanceMax = 450;
    } else if (graph.nodes.length > 150 && graph.nodes.length < 200) {
        strength = -1000; distanceMax = 500;
    } else if (graph.nodes.length > 200) {
        strength = -1600; distanceMax = 500;theta = 0.6,distance = 100,colideRadius = 35;
    }
    // 根据围绕数量调节
    if(_maxChildrenLength > 50 && _maxChildrenLength < 100){
        strength = -2000; distanceMax = 500;
    } else if(_maxChildrenLength > 1000 && _maxChildrenLength < 2000) {
        strength = -4000; distanceMax = 1500;
    }

    d3.forceSimulation(graph.nodes)
        .force('charge', d3.forceManyBody().strength(strength).distanceMax(distanceMax).theta(theta))
        .force('link', d3.forceLink(graph.layoutLinks).distance(distance))
        .force('center', d3.forceCenter(width / 2, height / 2))
        .force('collide', d3.forceCollide().radius(function () { return colideRadius;}))
    //.on('tick',ticked);
}

/** d3 svg */
/*var svg = d3.select('svg');
svg.selectAll('g').remove();// 清空
var svg_g = svg.append("g")

// 结点
var svg_nodes = svg_g.selectAll('circle')
    .enter().append('circle')
    .attr('r', function (d) {
        if(d.data.obj.labels[0] == 'Company'){
            return 33;
        } else {
            return 24;
        }
    })
    .attr('fill', function(d, i) {
        return d.data.color;
    })
    .style('opacity',1)*/
/** end d3 svg */


/*function ticked() {
    svg_nodes.attr("cx", function(d) {  return d.x; })
        .attr("cy", function(d) { return d.y; });
}*/

//设置符合Layout的node
function getLayoutNode(graphData) {
    var layoutNode = { current : _rootNode, level1 : [], level2 : [], level3 : [], level4 : [], level5 : [],other:[]};

    graphData.nodes.forEach(function (node,i) {
        switch (node.layout.level) {
            case 1: layoutNode.level1.push(node);break;
            case 2: layoutNode.level2.push(node);break;
            case 3: layoutNode.level3.push(node);break;
            case 4: layoutNode.level4.push(node);break;
            case 5: layoutNode.level5.push(node);break;
            default:layoutNode.other.push(node);break;
        }
    });

    _layoutNode = layoutNode;

    return layoutNode;
}
//将rootData转换成cy图谱框架所需要的数据结构
function transformData(graphData) {
    function getLinkColor(type) {
        if(type == '1'){
            return _COLOR.line.invest;
        } else if(type == '2') {
            return _COLOR.line.employ;
        } else {
            return _COLOR.line.legal;
        }
    }
    function getLinkLabel(link) {
        var type = link.data.obj.type, role = link.data.obj.properties.role;
        if(type == '1'){
            return '投资';
        } else if(type == '2') {
            return (role ? role : '任职');
        } else  {
            return role;
        }
    }
    //getLayoutNode(graphData);

    //
    id = graphData.nodes[0].nodeId;
    var els = {};
    els.nodes = [];
    els.edges = [];

    graphData.links.forEach(function (link,i) {
        var color = getLinkColor(link.data.obj.type);
        var label = getLinkLabel(link);

        els.edges.push({
            data:{
                data:link.data,
                color: color,
                id:link.linkId,
                label:label,
                source:link.sourceNode.nodeId,
                target:link.targetNode.nodeId
            },
            classes:'autorotate'
        });
    });

    graphData.nodes.forEach(function (node) {
        els.nodes.push({
            data:{
                nodeId:node.nodeId,
                type:node.data.obj.labels[0],
                /*keyNo:node.data.obj.properties.keyNo,*/  /*keyNo字段取消*/
                data:node.data,
                id:node.nodeId,
                name:node.data.obj.properties.name,
                category:node.data.category,
                color:node.data.color,
                borderColor:node.data.strokeColor,
                layout:node.layout,
                d3x:node.x,
                d3y:node.y,

                /*hasImage:node.data.obj.properties.hasImage,*/
                //labelLine:1 // 解决文字行距问题，第1行
            }
        });
    });
    return els;
}
// 图谱、筛选面板更新
function domUpdate(graphData) {
    getD3Position(graphData);

    setTimeout(function () {
        drawGraph(transformData(graphData));
    },500);
    selPanelUpdateList(graphData.nodes,graphData.links,true);
}

//截图2
function downImg(imgdata){
    var type = 'png';
    //将mime-type改为image/octet-stream,强制让浏览器下载
    var fixtype = function (type) {
        type = type.toLocaleLowerCase().replace(/jpg/i, 'jpeg');
        var r = type.match(/png|jpeg|bmp|gif/)[0];
        return 'image/' + r;
    }
    imgdata = imgdata.replace(fixtype(type), 'image/octet-stream')
    //将图片保存到本地
    var saveFile = function (data, filename) {
        var link = document.createElement('a');
        link.href = data;
        link.download = filename;
        var event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(event);
    }
    var filename = new Date().toLocaleDateString() + '.' + type;
    saveFile(imgdata, filename);
}
//截图2 IE
function downloadimgIE(canvas) {
    function post(URL, PARAMS) {
        var temp = document.createElement("form");
        temp.action = URL;
        temp.enctype = "multipart/form-data";
        temp.method = "post";
        temp.style.display = "none";
        for (var x in PARAMS) {
            var opt = document.createElement("textarea");
            opt.name = x;
            opt.value = PARAMS[x];
            temp.appendChild(opt);
        }
        document.body.appendChild(temp);
        temp.submit();
        return temp;
    }

    var qual = 1;
    if(canvas.width>3000){
        qual = 0.5;
    }else if(canvas.width>5000){
        qual = 0.4;
    }
    //设置保存图片的类型
    var imgdata = canvas.toDataURL('image/jpeg',qual);
    //var filename = '{{$smarty.get.name}}的关联图谱_'+new Date().toLocaleDateString() + '.jpeg';
    // var filename = _FILENAME + '的关联图谱.png';
    var filename = '企查查_图谱.png';
    post(INDEX_URL+'cms_downloadimg?filename='+filename, {img:imgdata});
}
//截图1
function canvasImg(imgData){
    var img = new Image();

    img.onload = function(e){

        var canvas = document.createElement('canvas');  //准备空画布
        canvas.width = img.width;
        canvas.height = img.height;
        var context = canvas.getContext('2d');  //取得画布的2d绘图上下文
        context.fillStyle = "#fff";
        context.fillRect(0,0,canvas.width,canvas.height);

        //画水印
        // var shuiying = new Image();
        // shuiying.src="/material/theme/chacha/cms/v2/images/shuiying2.png";
        // if(canvas.width>320){
        //     context.drawImage(shuiying, canvas.width/2-160, canvas.height/2-80,320,160);
        // }else{
        //     context.drawImage(shuiying, canvas.width/2-80, canvas.height/2-40,160,80);
        // }

        var shuiying = new Image();
/* 图片找不到
        shuiying.src="/material/theme/chacha/cms/v2/images/shuiying2.png";
*/
        for(var i=0;i < canvas.width+100; i+=600){
            for(var j=0; j< canvas.height+100; j+=456){
                context.drawImage(shuiying, i, j);
            }
        }

        //画图谱
        context.drawImage(img, 0, 0);

        if(canvas.width>400){
            var marker = '关联图谱由企查查基于公开信息利用大数据分析引擎独家生成';
            context.font = "28px 微软雅黑";
            context.fillStyle = "#aaaaaa";
            context.fillText(marker, canvas.width/2-context.measureText(marker).width/2, canvas.height-30);
        }

        downloadimgIE(canvas);

        /*if(!!window.ActiveXObject || "ActiveXObject" in window){ // ie
            context.drawImage(shuiying, canvas.width/2-160, canvas.height/2-80,320,160);
            downloadimgIE(canvas);
        } else {
            downImg(canvas.toDataURL('image/jpeg',1));
        }*/
    }

    img.src = imgData;
}


function getData(name,param){  /*keyNo字段取消*/
    var defaultParam = {
        //keyNo:keyNo,
       /* keyNo:"ff9480d03704349e0e6f819480afa94e",*/ /*keyNo字段取消*/
        name:"宜春津核环保科技有限公司",
    }
    /*keyNo字段取消*/
    /*if( keyNo.substr(0, 1) == "p"){
        defaultParam.startLabel = 'Person';
    }*/

    param = $.extend(defaultParam,param);
    $("#load_data").show();

    var url = INDEX_URL + '/company_muhouPersonAction';
    if(_TUPU_URL){
        url = _TUPU_URL;
    }
    if(typeof _INSERT_URL != 'undefined' && _INSERT_URL){
        url = _INSERT_URL;
    }
    // add by chenez for 静态数据代替动态数据 at 2018/9/25 14:41 start
    re = "{\"code\":200,\"data\":{\"nodes\":[{\"id\":\"140567397\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u6838\u71c3\u6599\u6709\u9650\u516c\u53f8\",\"registCapi\":\"353073\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"147432268\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u6c47\u80fd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"154844\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"102389276\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u5e7f\u4e1c\u4e1c\u65b9\u9506\u4e1a\u79d1\u6280\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"62094.6\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"98291676\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e0a\u6d77\u9ad8\u6cf0\u7cbe\u5bc6\u7ba1\u6750\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"14320\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"144215440\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u897f\u90e8\u65b0\u9506\u6838\u6750\u6599\u79d1\u6280\u6709\u9650\u516c\u53f8\",\"registCapi\":\"23000\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"134314506\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u534e\u9f99\u56fd\u9645\u6838\u7535\u6280\u672f\u6709\u9650\u516c\u53f8\",\"registCapi\":\"50000\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"137928757\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u897f\u5317\u6709\u8272\u91d1\u5c5e\u7814\u7a76\u9662\",\"registCapi\":\"10852\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"146279734\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u94c0\u4e1a\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"67900\",\"econKind\":\"\u91c7\u77ff\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"99158246\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u957f\u6c5f\u7535\u529b\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"2200000\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"97633984\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u91d1\u539f\u94c0\u4e1a\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":null,\"econKind\":null,\"status\":\"\u5728\u8425\"}},{\"id\":\"21838942\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u7530\u632f\u4e1a\"}},{\"id\":\"146851923\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838%28\u5317\u4eac%29\u4f20\u5a92\u6587\u5316\u6709\u9650\u516c\u53f8\",\"registCapi\":\"668\",\"econKind\":\"\u6587\u5316\u3001\u4f53\u80b2\u548c\u5a31\u4e50\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"145707707\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u878d\u8d44\u79df\u8d41\u6709\u9650\u516c\u53f8\",\"registCapi\":\"100000\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"143818374\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u6210\u90fd\u534e\u897f\u5bbe\u9986\u6709\u9650\u516c\u53f8\",\"registCapi\":\"617\",\"econKind\":\"\u4f4f\u5bbf\u548c\u9910\u996e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"105689178\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u9655\u897f\u4e94\u4e8c\u56db\u6838\u8bbe\u5907\u5236\u9020\u6709\u9650\u516c\u53f8\",\"registCapi\":\"4989.72\",\"econKind\":\"\u5c45\u6c11\u670d\u52a1\u3001\u4fee\u7406\u548c\u5176\u4ed6\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"143741574\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u534e\u590f\u8bc1\u5238\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"270057.8531\",\"econKind\":\"\u91d1\u878d\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"103782579\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56db\u5ddd\u5ce8\u8fb9\u897f\u6cb3\u6c34\u7535\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"5000\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"144567296\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u56db\u5ddd\u73af\u4fdd\u5de5\u7a0b\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"10500\",\"econKind\":\"\u6c34\u5229\u3001\u73af\u5883\u548c\u516c\u5171\u8bbe\u65bd\u7ba1\u7406\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"146047786\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56fd\u5bb6\u6838\u7535\u6280\u672f\u6709\u9650\u516c\u53f8\",\"registCapi\":\"2517152.143191\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"10016647\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u5f20\u5efa\u519b\"}},{\"id\":\"144214151\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u897f\u5b89\u6838\u8bbe\u5907\u6709\u9650\u516c\u53f8\",\"registCapi\":\"22001\",\"econKind\":\"\u4fe1\u606f\u4f20\u8f93\u3001\u8f6f\u4ef6\u548c\u4fe1\u606f\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"115443009\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838%28\u5929\u6d25%29\u673a\u68b0\u6709\u9650\u516c\u53f8\",\"registCapi\":\"10000\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"151997999\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u5b9d\u539f\u6295\u8d44\u6709\u9650\u516c\u53f8\",\"registCapi\":\"75725.53\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"111728521\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838%28\u6c55\u5934%29\u7cbe\u5bc6\u5236\u9020\u6709\u9650\u516c\u53f8\",\"registCapi\":\"600\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"100298205\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u4e2d\u539f\u5bf9\u5916\u5de5\u7a0b\u6709\u9650\u516c\u53f8\",\"registCapi\":\"24422.212123\",\"econKind\":\"\u5efa\u7b51\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"120297091\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u7b2c\u56db\u7814\u7a76\u8bbe\u8ba1\u5de5\u7a0b\u6709\u9650\u516c\u53f8\",\"registCapi\":\"13312.67\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"132860481\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e50\u5c71\u5e02\u91d1\u53e3\u6cb3\u65b0\u6751\u7535\u529b\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"1153.5\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"105782885\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56db\u5ddd\u7ea2\u534e\u5b9e\u4e1a\u6709\u9650\u516c\u53f8\",\"registCapi\":\"94375.69\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"31207287\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u7530\u950b\"}},{\"id\":\"122864126\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u6838\u80fd\u7535\u529b\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"1556543\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"68137250\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u7126\u6210\u8944\"}},{\"id\":\"108591575\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e50\u5c71\u5e02\u91d1\u53e3\u6cb3\u533a\u6cb3\u53e3\u7535\u7ad9\",\"registCapi\":\"395\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"118864954\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u4eba\u5bff\u4fdd\u9669\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"2826470.5\",\"econKind\":\"\u91d1\u878d\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"134283822\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u73af\u4fdd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"50000\",\"econKind\":\"\u6c34\u5229\u3001\u73af\u5883\u548c\u516c\u5171\u8bbe\u65bd\u7ba1\u7406\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"145552030\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u6838\u5de5\u4e1a\u96c6\u56e2\u516c\u53f8\u4e03\u4e94\u56db\u77ff\",\"registCapi\":\"395\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"148205927\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u79e6\u7687\u5c9b\u6838\u57ce\u65ed\u817e\u521b\u4e1a\u670d\u52a1\u6709\u9650\u516c\u53f8\",\"registCapi\":\"248.81\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"126867388\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e50\u5c71\u7ea2\u534e\u5efa\u5b89\u5de5\u7a0b\u6709\u9650\u516c\u53f8\",\"registCapi\":\"4500\",\"econKind\":\"\u5efa\u7b51\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"118776231\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u7518\u8083\u5170\u6838\u7535\u63a7\u6709\u9650\u516c\u53f8\",\"registCapi\":\"3139.0432\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"109920440\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u7518\u8083\u67f4\u5bb6\u5ce1\u6c34\u7535\u6709\u9650\u516c\u53f8\",\"registCapi\":\"20000\",\"econKind\":\"\u6c34\u5229\u3001\u73af\u5883\u548c\u516c\u5171\u8bbe\u65bd\u7ba1\u7406\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"137713667\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u9655\u94c0\u6c49\u4e2d\u673a\u7535\u8bbe\u5907\u5236\u9020\u6709\u9650\u516c\u53f8\",\"registCapi\":\"2952\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"62729482\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u5468\u519b\"}},{\"id\":\"106582627\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u6838\u7535\u5de5\u7a0b\u6709\u9650\u516c\u53f8\",\"registCapi\":\"28000\",\"econKind\":\"\u5efa\u7b51\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"124552533\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e0a\u6d77\u4e2d\u6838\u6d66\u539f\u6709\u9650\u516c\u53f8\",\"registCapi\":\"15800\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"148364910\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u5b9c\u5bbe\u5efa\u4e2d\u8fd0\u4e1a\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"600\",\"econKind\":\"\u4ea4\u901a\u8fd0\u8f93\u3001\u4ed3\u50a8\u548c\u90ae\u653f\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"103731095\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u8fdc\u6d77\u8fd0\u63a7\u80a1\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"1021627.4357\",\"econKind\":\"\u4ea4\u901a\u8fd0\u8f93\u3001\u4ed3\u50a8\u548c\u90ae\u653f\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"118867759\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u94c0\u4e1a\u6709\u9650\u516c\u53f8\",\"registCapi\":\"106186.26\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"149717463\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838%28\u4e0a\u6d77%29\u4f01\u4e1a\u53d1\u5c55\u6709\u9650\u516c\u53f8\",\"registCapi\":\"237000\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"130001027\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838%28\u5c71\u897f%29\u6838\u4e03\u9662\u76d1\u7406\u6709\u9650\u516c\u53f8\",\"registCapi\":\"500\",\"econKind\":\"\u5efa\u7b51\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"130572042\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u71c3\u6599\u6ca7\u5dde\u6709\u9650\u516c\u53f8\",\"registCapi\":\"49622\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"151430724\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u56db0\u56db\u6709\u9650\u516c\u53f8\",\"registCapi\":\"87483.47\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"107152416\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u4fe1\u91d1\u5c5e\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"439884.6153\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"133426736\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u5efa\u4e2d\u6838\u71c3\u6599\u5143\u4ef6\u6709\u9650\u516c\u53f8\",\"registCapi\":\"63858.9955\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"103730667\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u9655\u94c0\u6c49\u4e2d\u5b9e\u4e1a\u6709\u9650\u516c\u53f8\",\"registCapi\":\"608.2755\",\"econKind\":\"\u4ea4\u901a\u8fd0\u8f93\u3001\u4ed3\u50a8\u548c\u90ae\u653f\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"106014743\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u65b0\u80fd\u6838\u5de5\u4e1a\u5de5\u7a0b\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"11000\",\"econKind\":\"\u5efa\u7b51\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"110584785\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u9655\u897f\u94c0\u6d53\u7f29\u6709\u9650\u516c\u53f8\",\"registCapi\":\"128192.82\",\"econKind\":\"\u91c7\u77ff\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"113155704\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u6e05\u539f\u73af\u5883\u6280\u672f\u5de5\u7a0b\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"5000\",\"econKind\":\"\u6c34\u5229\u3001\u73af\u5883\u548c\u516c\u5171\u8bbe\u65bd\u7ba1\u7406\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"110584784\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u8d22\u52a1\u6709\u9650\u8d23\u4efb\u516c\u53f8\",\"registCapi\":\"401920\",\"econKind\":\"\u91d1\u878d\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"106089626\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u5b9c\u5bbe\u5efa\u4e2d\u6280\u672f\u54a8\u8be2\u670d\u52a1\u90e8\",\"registCapi\":\"5.86\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"23292463\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u674e\u4e2d\u594e\"}},{\"id\":\"126858691\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u6838\u5de5\u4e1a\u96c6\u56e2\u6709\u9650\u516c\u53f8\",\"registCapi\":\"5200000\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"133709494\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u534e\u5b87\u7ecf\u6d4e\u53d1\u5c55\u6709\u9650\u516c\u53f8\",\"registCapi\":\"347\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"101159249\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u9655\u94c0\u6c49\u4e2d\u5de5\u7a0b\u6709\u9650\u516c\u53f8\",\"registCapi\":\"5100\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"150573985\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u5305\u5934\u6838\u71c3\u6599\u5143\u4ef6\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"60000\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"131480207\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56db\u5ddd\u5ce8\u7709\u5c71\u7535\u529b\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"14127.5516\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"75385252\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u5f20\u5c0f\u7fa4\"}},{\"id\":\"103445104\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u9f99\u539f\u79d1\u6280\u6709\u9650\u516c\u53f8\",\"registCapi\":\"5000\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"109462362\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u5317\u4eac\u4e2d\u6838\u5357\u793c\u58eb\u8def\u5bbe\u9986\",\"registCapi\":\"2920\",\"econKind\":\"\u4f4f\u5bbf\u548c\u9910\u996e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"128594422\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u5317\u4eac\u4e2d\u6838\u4ea7\u4e1a\u6295\u8d44\u57fa\u91d1%28\u6709\u9650\u5408\u4f19%29\",\"registCapi\":\"122000\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"150454826\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u6df1\u5733\u4e2d\u6838\u96c6\u56e2\u6709\u9650\u516c\u53f8\",\"registCapi\":\"24695\",\"econKind\":\"\u623f\u5730\u4ea7\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"143139797\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u82cf\u9600\u79d1\u6280\u5b9e\u4e1a\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"38341.7593\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"109728084\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u5730\u8d28\u52d8\u67e5\u7ba1\u7406\u6709\u9650\u516c\u53f8\",\"registCapi\":\"1000\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"121725471\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u745e\u80fd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"11000\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"135142352\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u7518\u8083\u98ce\u529b\u53d1\u7535\u6709\u9650\u516c\u53f8\",\"registCapi\":\"8000\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"140679741\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u6210\u90fd\u5efa\u4e2d\u9502\u7535\u6c60\u6709\u9650\u516c\u53f8\",\"registCapi\":\"2200\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"141713325\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u76d0\u534e\u6e58\u5316\u5de5\u6709\u9650\u516c\u53f8\",\"registCapi\":\"17500\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"105154464\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u540c\u8f90\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"23990.61\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"144940229\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u5e7f\u4e1c\u4e9a\u4eff\u79d1\u6280\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"10500\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"99784724\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56fd\u6cf0\u541b\u5b89\u6295\u8d44\u7ba1\u7406\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"137583\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"154002884\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u4ea7\u4e1a\u57fa\u91d1\u7ba1\u7406%28\u5317\u4eac%29\u6709\u9650\u516c\u53f8\",\"registCapi\":\"12000\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"99445766\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u65b0\u80fd\u6e90\u6709\u9650\u516c\u53f8\",\"registCapi\":\"10000\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"128270410\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e0a\u6d77\u664b\u72c4\u5efa\u7b51\u8bbe\u8ba1\u6709\u9650\u516c\u53f8\",\"registCapi\":\"100\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"114923446\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56db\u5ddd\u6c38\u4e50\u7535\u529b\u6709\u9650\u516c\u53f8\",\"registCapi\":\"10000\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"103441496\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u539f\u5b50\u80fd\u5de5\u4e1a\u6709\u9650\u516c\u53f8\",\"registCapi\":\"6459.1\",\"econKind\":\"\u6279\u53d1\u548c\u96f6\u552e\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"97786531\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u56fd\u52a1\u9662\u56fd\u6709\u8d44\u4ea7\u76d1\u7763\u7ba1\u7406\u59d4\u5458\u4f1a\",\"registCapi\":null,\"econKind\":null,\"status\":\"\u5728\u8425\"}},{\"id\":\"129360333\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u897f\u5b89\u5de5\u4e1a\u6295\u8d44\u96c6\u56e2\u6709\u9650\u516c\u53f8\",\"registCapi\":\"154969.775076\",\"econKind\":\"\u79df\u8d41\u548c\u5546\u52a1\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"112695247\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u6210\u90fd\u5efa\u4e2d\u9999\u6599\u9999\u7cbe\u6709\u9650\u516c\u53f8\",\"registCapi\":\"3510\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"138284103\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u5de5\u7a0b\u54a8\u8be2\u6709\u9650\u516c\u53f8\",\"registCapi\":\"5000\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"130701235\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u6838\u5de5\u4e1a\u7b2c\u4e8c\u7814\u7a76\u8bbe\u8ba1\u9662\",\"registCapi\":\"785.3\",\"econKind\":\"\u79d1\u5b66\u7814\u7a76\u548c\u6280\u672f\u670d\u52a1\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"2025828\",\"labels\":[\"Person\"],\"properties\":{\"name\":\"\u738b\u6587\u751f\"}},{\"id\":\"96820025\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u534f\u548c\u6e2f\u6709\u9650\u516c\u53f8\",\"registCapi\":null,\"econKind\":null,\"status\":\"\u5728\u8425\"}},{\"id\":\"136856918\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u6838\u5170\u5dde\u94c0\u6d53\u7f29\u6709\u9650\u516c\u53f8\",\"registCapi\":\"155948\",\"econKind\":\"\u5236\u9020\u4e1a\",\"status\":\"\u5728\u8425\"}},{\"id\":\"148001709\",\"labels\":[\"Company\"],\"properties\":{\"name\":\"\u4e2d\u56fd\u5e7f\u6838\u7535\u529b\u80a1\u4efd\u6709\u9650\u516c\u53f8\",\"registCapi\":\"4544875\",\"econKind\":\"\u7535\u529b\u3001\u70ed\u529b\u3001\u71c3\u6c14\u53ca\u6c34\u751f\u4ea7\u548c\u4f9b\u5e94\u4e1a\",\"status\":\"\u5728\u8425\"}}],\"relationships\":[{\"id\":\"319630508\",\"startNode\":\"105782885\",\"endNode\":\"108591575\",\"type\":1,\"properties\":{\"stockPercent\":34.06,\"shouldcapi\":\"134.5405\"}},{\"id\":\"318596926\",\"startNode\":\"140567397\",\"endNode\":\"105782885\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"94375.69\"}},{\"id\":\"317377496\",\"startNode\":\"106582627\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":8,\"shouldcapi\":\"18960\"}},{\"id\":\"318217508\",\"startNode\":\"106014743\",\"endNode\":\"130001027\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"500\"}},{\"id\":\"319459812\",\"startNode\":\"126858691\",\"endNode\":\"128594422\",\"type\":1,\"properties\":{\"stockPercent\":49.18,\"shouldcapi\":\"60000\"}},{\"id\":\"319828695\",\"startNode\":\"140567397\",\"endNode\":\"136856918\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"155948\"}},{\"id\":\"321746569\",\"startNode\":\"96820025\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":25,\"shouldcapi\":\"25000\"}},{\"id\":\"320701741\",\"startNode\":\"105782885\",\"endNode\":\"126867388\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"4500\"}},{\"id\":\"319118010\",\"startNode\":\"105782885\",\"endNode\":\"137713667\",\"type\":1,\"properties\":{\"stockPercent\":3.39,\"shouldcapi\":\"100\"}},{\"id\":\"319137556\",\"startNode\":\"110584785\",\"endNode\":\"115443009\",\"type\":1,\"properties\":{\"stockPercent\":14,\"shouldcapi\":\"1400\"}},{\"id\":\"57060229\",\"startNode\":\"68137250\",\"endNode\":\"130572042\",\"type\":2,\"properties\":{\"role\":\"\u6cd5\u4eba\"}},{\"id\":\"319734389\",\"startNode\":\"110584785\",\"endNode\":\"103730667\",\"type\":1,\"properties\":{\"stockPercent\":13.83,\"shouldcapi\":\"84.0979\"}},{\"id\":\"163636186\",\"startNode\":\"68137250\",\"endNode\":\"140567397\",\"type\":2,\"properties\":{\"role\":\"\u6cd5\u4eba\"}},{\"id\":\"318226654\",\"startNode\":\"126858691\",\"endNode\":\"141713325\",\"type\":1,\"properties\":{\"stockPercent\":40,\"shouldcapi\":\"7000\"}},{\"id\":\"22715123\",\"startNode\":\"68137250\",\"endNode\":\"140567397\",\"type\":2,\"properties\":{\"role\":\"\u7ecf\u7406\"}},{\"id\":\"317633002\",\"startNode\":\"126858691\",\"endNode\":\"146279734\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"67900\"}},{\"id\":\"320213826\",\"startNode\":\"136856918\",\"endNode\":\"109920440\",\"type\":1,\"properties\":{\"stockPercent\":50,\"shouldcapi\":\"10000\"}},{\"id\":\"320336187\",\"startNode\":\"126858691\",\"endNode\":\"143741574\",\"type\":1,\"properties\":{\"stockPercent\":0.57,\"shouldcapi\":\"1539.3298\"}},{\"id\":\"320315905\",\"startNode\":\"126858691\",\"endNode\":\"102389276\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"320384409\",\"startNode\":\"105782885\",\"endNode\":\"110584784\",\"type\":1,\"properties\":{\"stockPercent\":0.7,\"shouldcapi\":\"2816\"}},{\"id\":\"320501753\",\"startNode\":\"147432268\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":1,\"shouldcapi\":\"1000\"}},{\"id\":\"319379823\",\"startNode\":\"126858691\",\"endNode\":\"146047786\",\"type\":1,\"properties\":{\"stockPercent\":3.97,\"shouldcapi\":\"100000\"}},{\"id\":\"317530714\",\"startNode\":\"133426736\",\"endNode\":\"110584784\",\"type\":1,\"properties\":{\"stockPercent\":1.23,\"shouldcapi\":\"4928\"}},{\"id\":\"318872298\",\"startNode\":\"140567397\",\"endNode\":\"110584785\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"128192.82\"}},{\"id\":\"319480820\",\"startNode\":\"126858691\",\"endNode\":\"143139797\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"319108579\",\"startNode\":\"133426736\",\"endNode\":\"137713667\",\"type\":1,\"properties\":{\"stockPercent\":3.39,\"shouldcapi\":\"100\"}},{\"id\":\"319558830\",\"startNode\":\"126858691\",\"endNode\":\"110584784\",\"type\":1,\"properties\":{\"stockPercent\":53.49,\"shouldcapi\":\"214976\"}},{\"id\":\"317203178\",\"startNode\":\"126858691\",\"endNode\":\"113155704\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"5000\"}},{\"id\":\"320122405\",\"startNode\":\"126858691\",\"endNode\":\"122864126\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"319477898\",\"startNode\":\"110584785\",\"endNode\":\"110584784\",\"type\":1,\"properties\":{\"stockPercent\":0.64,\"shouldcapi\":\"2560\"}},{\"id\":\"317688575\",\"startNode\":\"126858691\",\"endNode\":\"133709494\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"347\"}},{\"id\":\"316795477\",\"startNode\":\"126858691\",\"endNode\":\"105689178\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"4989.72\"}},{\"id\":\"320486676\",\"startNode\":\"126858691\",\"endNode\":\"144567296\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"10500\"}},{\"id\":\"316572779\",\"startNode\":\"122864126\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":28,\"shouldcapi\":\"66360\"}},{\"id\":\"319697353\",\"startNode\":\"140567397\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":4.5,\"shouldcapi\":\"4500\"}},{\"id\":\"310842961\",\"startNode\":\"31207287\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":2.17,\"shouldcapi\":\"500\"}},{\"id\":\"316631192\",\"startNode\":\"124552533\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":35.78,\"shouldcapi\":\"84800\"}},{\"id\":\"320321656\",\"startNode\":\"126858691\",\"endNode\":\"140567397\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"353073\"}},{\"id\":\"318531698\",\"startNode\":\"126858691\",\"endNode\":\"150454826\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"24695\"}},{\"id\":\"316969284\",\"startNode\":\"126858691\",\"endNode\":\"120297091\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"13312.67\"}},{\"id\":\"316419233\",\"startNode\":\"136856918\",\"endNode\":\"118776231\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"3139.0432\"}},{\"id\":\"317016318\",\"startNode\":\"124552533\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":5,\"shouldcapi\":\"5000\"}},{\"id\":\"317470369\",\"startNode\":\"126858691\",\"endNode\":\"106582627\",\"type\":1,\"properties\":{\"stockPercent\":96.43,\"shouldcapi\":\"27000\"}},{\"id\":\"317641341\",\"startNode\":\"126858691\",\"endNode\":\"118864954\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"57060228\",\"startNode\":\"68137250\",\"endNode\":\"130572042\",\"type\":2,\"properties\":{\"role\":\"\u6267\u884c\u8463\u4e8b\"}},{\"id\":\"320913872\",\"startNode\":\"140567397\",\"endNode\":\"151430724\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"87483.47\"}},{\"id\":\"319451398\",\"startNode\":\"126858691\",\"endNode\":\"138284103\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"5000\"}},{\"id\":\"316353810\",\"startNode\":\"106582627\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":7.5,\"shouldcapi\":\"7500\"}},{\"id\":\"319028543\",\"startNode\":\"145707707\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":2.11,\"shouldcapi\":\"5000\"}},{\"id\":\"317089813\",\"startNode\":\"126858691\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":31,\"shouldcapi\":\"31000\"}},{\"id\":\"317474205\",\"startNode\":\"126858691\",\"endNode\":\"144214151\",\"type\":1,\"properties\":{\"stockPercent\":61.23,\"shouldcapi\":\"13471\"}},{\"id\":\"317137526\",\"startNode\":\"126858691\",\"endNode\":\"144940229\",\"type\":1,\"properties\":{\"stockPercent\":9.52,\"shouldcapi\":\"1000\"}},{\"id\":\"318411624\",\"startNode\":\"133426736\",\"endNode\":\"98291676\",\"type\":1,\"properties\":{\"stockPercent\":59.93,\"shouldcapi\":\"8582\"}},{\"id\":\"318680871\",\"startNode\":\"140567397\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":34.78,\"shouldcapi\":\"8000\"}},{\"id\":\"317088722\",\"startNode\":\"136856918\",\"endNode\":\"110584784\",\"type\":1,\"properties\":{\"stockPercent\":1.4,\"shouldcapi\":\"5632\"}},{\"id\":\"318401997\",\"startNode\":\"126858691\",\"endNode\":\"134283822\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"50000\"}},{\"id\":\"319944057\",\"startNode\":\"133426736\",\"endNode\":\"111728521\",\"type\":1,\"properties\":{\"stockPercent\":51,\"shouldcapi\":\"306\"}},{\"id\":\"316434446\",\"startNode\":\"126858691\",\"endNode\":\"103441496\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"6459.1\"}},{\"id\":\"316355539\",\"startNode\":\"133426736\",\"endNode\":\"143818374\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"617\"}},{\"id\":\"317782497\",\"startNode\":\"105154464\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":4,\"shouldcapi\":\"4000\"}},{\"id\":\"319342349\",\"startNode\":\"100298205\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":7.5,\"shouldcapi\":\"7500\"}},{\"id\":\"316870000\",\"startNode\":\"126858691\",\"endNode\":\"154002884\",\"type\":1,\"properties\":{\"stockPercent\":66.67,\"shouldcapi\":\"8000\"}},{\"id\":\"320606115\",\"startNode\":\"133426736\",\"endNode\":\"148364910\",\"type\":1,\"properties\":{\"stockPercent\":38,\"shouldcapi\":\"228\"}},{\"id\":\"316500625\",\"startNode\":\"126858691\",\"endNode\":\"118867759\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"106186.26\"}},{\"id\":\"319562338\",\"startNode\":\"126858691\",\"endNode\":\"121725471\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"11000\"}},{\"id\":\"320853628\",\"startNode\":\"110584785\",\"endNode\":\"101159249\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"5100\"}},{\"id\":\"317886048\",\"startNode\":\"133426736\",\"endNode\":\"140679741\",\"type\":1,\"properties\":{\"stockPercent\":64.7,\"shouldcapi\":\"1423.5\"}},{\"id\":\"321049622\",\"startNode\":\"126858691\",\"endNode\":\"100298205\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"24422.212123\"}},{\"id\":\"318642854\",\"startNode\":\"129360333\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":13.04,\"shouldcapi\":\"3000\"}},{\"id\":\"318691725\",\"startNode\":\"126858691\",\"endNode\":\"109728084\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"1000\"}},{\"id\":\"317467390\",\"startNode\":\"126858691\",\"endNode\":\"151997999\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"75725.53\"}},{\"id\":\"320241261\",\"startNode\":\"100298205\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":8,\"shouldcapi\":\"18960\"}},{\"id\":\"320068957\",\"startNode\":\"140567397\",\"endNode\":\"133426736\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"63858.9955\"}},{\"id\":\"22715124\",\"startNode\":\"68137250\",\"endNode\":\"140567397\",\"type\":2,\"properties\":{\"role\":\"\u6267\u884c\u8463\u4e8b\"}},{\"id\":\"317203499\",\"startNode\":\"126858691\",\"endNode\":\"103445104\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"5000\"}},{\"id\":\"319185190\",\"startNode\":\"126858691\",\"endNode\":\"148001709\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"319772701\",\"startNode\":\"126858691\",\"endNode\":\"99784724\",\"type\":1,\"properties\":{\"stockPercent\":0.47,\"shouldcapi\":\"650.6678\"}},{\"id\":\"317112423\",\"startNode\":\"105782885\",\"endNode\":\"131480207\",\"type\":1,\"properties\":{\"stockPercent\":2.04,\"shouldcapi\":\"288.32\"}},{\"id\":\"318904453\",\"startNode\":\"136856918\",\"endNode\":\"115443009\",\"type\":1,\"properties\":{\"stockPercent\":14,\"shouldcapi\":\"1400\"}},{\"id\":\"320955250\",\"startNode\":\"126858691\",\"endNode\":\"134314506\",\"type\":1,\"properties\":{\"stockPercent\":50,\"shouldcapi\":\"25000\"}},{\"id\":\"242596427\",\"startNode\":\"62729482\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":2.17,\"shouldcapi\":\"500\"}},{\"id\":\"319671982\",\"startNode\":\"126858691\",\"endNode\":\"99158246\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"316657205\",\"startNode\":\"126858691\",\"endNode\":\"109462362\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"2920\"}},{\"id\":\"319904965\",\"startNode\":\"105782885\",\"endNode\":\"132860481\",\"type\":1,\"properties\":{\"stockPercent\":76.28,\"shouldcapi\":\"879.9\"}},{\"id\":\"317855395\",\"startNode\":\"126858691\",\"endNode\":\"103731095\",\"type\":1,\"properties\":{\"stockPercent\":0,\"shouldcapi\":null}},{\"id\":\"319887893\",\"startNode\":\"106014743\",\"endNode\":\"137713667\",\"type\":1,\"properties\":{\"stockPercent\":3.39,\"shouldcapi\":\"100\"}},{\"id\":\"317798884\",\"startNode\":\"151997999\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":2.11,\"shouldcapi\":\"5000\"}},{\"id\":\"316380503\",\"startNode\":\"107152416\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":4.35,\"shouldcapi\":\"1000\"}},{\"id\":\"320786348\",\"startNode\":\"106014743\",\"endNode\":\"128270410\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"100\"}},{\"id\":\"318394658\",\"startNode\":\"137928757\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":33.91,\"shouldcapi\":\"7800\"}},{\"id\":\"318315680\",\"startNode\":\"110584785\",\"endNode\":\"137713667\",\"type\":1,\"properties\":{\"stockPercent\":77.91,\"shouldcapi\":\"2300\"}},{\"id\":\"320612272\",\"startNode\":\"105782885\",\"endNode\":\"114923446\",\"type\":1,\"properties\":{\"stockPercent\":10,\"shouldcapi\":\"1000\"}},{\"id\":\"271902824\",\"startNode\":\"10016647\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":2.17,\"shouldcapi\":\"500\"}},{\"id\":\"300928261\",\"startNode\":\"21838942\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":2.17,\"shouldcapi\":\"500\"}},{\"id\":\"265529760\",\"startNode\":\"2025828\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":2.17,\"shouldcapi\":\"500\"}},{\"id\":\"317716018\",\"startNode\":\"126858691\",\"endNode\":\"146851923\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"668\"}},{\"id\":\"176733618\",\"startNode\":\"68137250\",\"endNode\":\"130572042\",\"type\":2,\"properties\":{\"role\":\"\u6cd5\u4eba\"}},{\"id\":\"320708747\",\"startNode\":\"126858691\",\"endNode\":\"145552030\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"395\"}},{\"id\":\"318126372\",\"startNode\":\"133426736\",\"endNode\":\"106089626\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"5.86\"}},{\"id\":\"320886665\",\"startNode\":\"105782885\",\"endNode\":\"103782579\",\"type\":1,\"properties\":{\"stockPercent\":70,\"shouldcapi\":\"3500\"}},{\"id\":\"320822062\",\"startNode\":\"126858691\",\"endNode\":\"130701235\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"785.3\"}},{\"id\":\"320836541\",\"startNode\":\"140567397\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":8,\"shouldcapi\":\"18960\"}},{\"id\":\"320993059\",\"startNode\":\"122864126\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":10,\"shouldcapi\":\"10000\"}},{\"id\":\"316412381\",\"startNode\":\"140567397\",\"endNode\":\"106014743\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"11000\"}},{\"id\":\"320244758\",\"startNode\":\"126858691\",\"endNode\":\"99445766\",\"type\":1,\"properties\":{\"stockPercent\":80,\"shouldcapi\":\"8000\"}},{\"id\":\"318947548\",\"startNode\":\"151430724\",\"endNode\":\"148205927\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"248.81\"}},{\"id\":\"320545624\",\"startNode\":\"126858691\",\"endNode\":\"147432268\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"154844\"}},{\"id\":\"319750063\",\"startNode\":\"103441496\",\"endNode\":\"149717463\",\"type\":1,\"properties\":{\"stockPercent\":8,\"shouldcapi\":\"18960\"}},{\"id\":\"275999035\",\"startNode\":\"23292463\",\"endNode\":\"144215440\",\"type\":1,\"properties\":{\"stockPercent\":3.04,\"shouldcapi\":\"700\"}},{\"id\":\"320688584\",\"startNode\":\"133426736\",\"endNode\":\"112695247\",\"type\":1,\"properties\":{\"stockPercent\":85.47,\"shouldcapi\":\"3000\"}},{\"id\":\"22715122\",\"startNode\":\"75385252\",\"endNode\":\"140567397\",\"type\":2,\"properties\":{\"role\":\"\u76d1\u4e8b\"}},{\"id\":\"322329210\",\"startNode\":\"97633984\",\"endNode\":\"145707707\",\"type\":1,\"properties\":{\"stockPercent\":4.5,\"shouldcapi\":\"4500\"}},{\"id\":\"321048026\",\"startNode\":\"133426736\",\"endNode\":\"150573985\",\"type\":1,\"properties\":{\"stockPercent\":15,\"shouldcapi\":\"9000\"}},{\"id\":\"319804199\",\"startNode\":\"151430724\",\"endNode\":\"135142352\",\"type\":1,\"properties\":{\"stockPercent\":15,\"shouldcapi\":\"1200\"}},{\"id\":\"321351432\",\"startNode\":\"97786531\",\"endNode\":\"126858691\",\"type\":1,\"properties\":{\"stockPercent\":100,\"shouldcapi\":\"5200000\"}}]}}";
    re=JSON.parse(re);
    /*大咖搜索地址栏补足keyNo*/
    /*if(window.location.pathname == '/company_muhouperson'){
        history.pushState('','','company_muhouperson?keyNo='+_currentKeyNo);
    }*/

    /*re = re.success;*/
    if( !re || re == undefined || !re.data || re.data.nodes.length == 0){
        $("#load_data").hide();
        $(".printLogo").hide();
        $(".tp-foot").hide();
        $("#Main").hide();
        $('#no_data').show();
        return;
    } else {
        $(".printLogo").show();
        $(".tp-foot").show();
        $("#Main").show();
        $('#no_data').hide();
    }

    _rootData = getRootData(re.data);

    domUpdate(_rootData);


/*    $.ajax({
        url:url,
        //url:'http://10.0.0.38:9600/api/Graph/GetCompanyGraph',
        type: 'GET',
        data:param,
        dataType: 'JSON',
        success: function (re){
            /!*大咖搜索地址栏补足keyNo*!/
            if(window.location.pathname == '/company_muhouperson'){
                history.pushState('','','company_muhouperson?keyNo='+_currentKeyNo);
            }

            re = re.success;

            if( !re || re.results == undefined || !re.results[0] || !re.results[0].data.length || re.results[0].data[0].graph.nodes.length == 0){
                $("#load_data").hide();
                $(".printLogo").hide();
                $(".tp-foot").hide();
                $("#Main").hide();
                $('#no_data').show();
                return;
            } else {
                $(".printLogo").show();
                $(".tp-foot").show();
                $("#Main").show();
                $('#no_data').hide();
            }

            _rootData = getRootData(re.results[0].data);

            domUpdate(_rootData);
        },error:function(data){
            $("#load_data").hide();
            $(".printLogo").hide();
            $(".tp-foot").hide();
            $("#Main").hide();
            $('#no_data').show();
        }
    });*/
// add by chenez for 静态数据代替动态数据  at 2018/9/25 14:41 end

}
//刷新
function refresh(name) {
    $('.company-detail').fadeOut();
    $('#MainCy').html('');
    /*_currentKeyNo = keyNo;*/   /*keyNo字段取消*/
    _currentName = name;
    $('#TrTxt').removeClass('active');
    /*getData(_currentKeyNo);*/  /*keyNo字段取消*/
    getData(_currentName);
    focusCancel();
    filterReset();

    //页面
    try{
        hideSearchBoxHide();
    }catch (e){}
}

/**详情弹窗*/
function showPerTupu(type){
    var canshow = $("#ShowPerTupu").attr('canshow');
    if(canshow){
        /*keyNo字段取消*/
        /*var keyNo = $("#ShowPerTupu").attr('keyno');
        refresh(keyNo);*/
        var name = $("#ShowPerTupu").attr('name');
        refresh(name);
    }
}
/*关闭详情*/
function popclose(dom){
    // add by chenez for 关闭公司详情 at 2018/9/26 11:35 start
    //简化处理
    $("#company-detail").fadeOut();
    //$(dom).parent().parent().fadeOut();
    // add by chenez for 关闭公司详情 at 2018/9/26 11:35 end
}
/*人物头像没有时处理*/
function personImgErr() {
    var name = $(".ea_name").text();
    $('#face_oss').hide();
    $('.ea_defaultImg').show();
    $('.ea_defaultImg').text(name[0]);
}
/**END 详情弹窗*/


window.onresize=function(){
    /*resizeScreen();*/
    printLogoFixed();
}
$(document).ready(function () {


    printLogoFixed();

    /*_currentKeyNo = getQueryString('keyNo');*/  /*keyNo字段取消*/
    _currentName = getQueryString('name');
    /*if(!_currentKeyNo){*/
    if (!_currentName){
        if(typeof _HOTPERSON != 'undefined' && _HOTPERSON){
            /*_currentKeyNo = _HOTPERSON;*/
            _currentName = _HOTPERSON;
        } else {
            /*_currentKeyNo = '';*/
            _currentName = '';
        }
    }

    /*getData(_currentKeyNo);*/
    getData(_currentName);


    /**筛选面板*/

    // 层级筛选
    $("#ShowLevel > a").click(function () {
        $('#ShowLevel > a').removeClass('active');
        $(this).addClass('active');

        var level = parseInt($(this).attr('level'));
        $('#SelPanel').attr('param-level',level);
        filter(_rootData);
    });//#ShowLevel
    // 状态筛选
    $("#ShowStatus > a").click(function () {
        $('#ShowStatus > a').removeClass('active');
        $(this).addClass('active');

        var status = $(this).attr('status');
        $('#SelPanel').attr('param-status',status);
        filter(_rootData);
    });//#ShowLevel
    // 持股筛选
    var inputEvent = (!!window.ActiveXObject || "ActiveXObject" in window) ? 'change' : 'input';
    $('#inputRange').bind(inputEvent, function(e){

        var value = $('#inputRange').val()+"%";
        console.log(value);
        $('#rangeValue').text(value);
        $('#inputRange').css('background-size', value + '% 100%' );
        $('#RangeLabel span').text(value + '%');

        $('#SelPanel').attr('param-num',value);
        filter(_rootData);
    });
    // 投资筛选
    $("#ShowInvest > a").click(function () {
        $('#ShowInvest > a').removeClass('active');
        $(this).addClass('active');

        var invest = $(this).attr('invest');
        $('#SelPanel').attr('param-invest',invest);
        filter(_rootData);
    });//#ShowLevel
    // 关闭
    $('.tp-sel-close span').click(function () {
        selPanelHide();
    });
    // 聚焦
    $('#FocusBt').click(function () {
        var status = $('#FocusBt').text();
        if(!$(this).hasClass('focusDisable')){
            if(status == '聚焦'){
                if(!$('#FocusInput').val()){
                    faldia({content:'请点击选取结点'});
                    return;
                }

                var nodeId = $('#FocusInput').attr('node_id')
                if(!nodeId){
                    return;
                } else {
                    $('#FocusBt').text('取消');
                    highLight([nodeId],cy);
                }
            } else if (status == '取消'){
                focusCancel();
            }
        }

    });
    // 输入框
    $('#FocusInput').keyup(function () {
        $('.tp-list').html('');
        var _this = $(this);
        var keyword = _this.val();

        if(keyword){
            $('#ClearInput').show();
        } else {
            $('#ClearInput').hide();
        }

        setTimeout(function () {
            var selNodes = [];
            _rootData.nodes.forEach(function (node) {
                var name = node.data.obj.properties.name;
                if(name.match(keyword)){
                    selNodes.push(node);
                }
            });

            selPanelUpdateList(selNodes,_rootData.links,false);
        },500);
    });
    $('#ClearInput').click(function () {
        focusCancel();
    });

    /**详情面板*/

    /*$('.tp-detail-close span').click(function () {
        //cancelHighLight();
        $('.tp-detail').fadeOut();
    });*/
    /*$('#ViewTupu').click(function () {
        var guid = $(this).attr('guid');
        init(guid);
    });*/

    /**侧边栏*/

    $('#TrSel').click(function () {
        var _this = $(this);
        if(_this.hasClass('active')){
            selPanelHide();
        } else {
            selPanelShow();
        }
    });
    /*$('#TrFullScreen').click(function () {
        var old = cy.pan();
        var distance = 60;
        if(isFullScreen()){
            cy.pan({
                x:old.x,
                y:old.y - distance
            });
            exitFullScreen();
        } else {
            cy.pan({
                x:old.x,
                y:old.y + distance
            });
            launchFullScreen($('#Main')[0]);
        }
    });*/
    //刷新按钮点击事件
    $('#TrRefresh').click(function () {
        /*refresh(_currentKeyNo);*/   /*keyNo字段取消*/
        refresh(_currentName);
    });
    //保存按钮点击事件
    $('#TrSave').click(function () {
        if(!$('#TrTxt').hasClass('active')){
            $('#TrTxt').click();
        }

        canvasImg(cy.png({full:true,bg:'#0000',scale:1.8}));
    });
});
// 获取url中的参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURIComponent(r[2]); return null;
}